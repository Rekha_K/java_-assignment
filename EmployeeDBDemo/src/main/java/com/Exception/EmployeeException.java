package com.Exception;

public class EmployeeException extends Exception {
	private String message;

	public EmployeeException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {

		return this.message;
	}

}
