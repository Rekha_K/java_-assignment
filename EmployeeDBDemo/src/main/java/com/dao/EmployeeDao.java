package com.dao;

import com.model.Employee;
/**
 * 
 * @author rekha.k
 *
 */
public interface EmployeeDao {

	public abstract Employee validateIdAndPassword(int Id);

}
