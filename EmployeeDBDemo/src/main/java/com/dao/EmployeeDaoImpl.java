package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.model.Employee;
/**
 * 
 * @author rekha.k
 *
 */
public class EmployeeDaoImpl implements EmployeeDao {
	// private static final Logger LOGGER =
	// LogManager.getLogger(EmployeeDaoImpl.class);

	@Override
	public Employee validateIdAndPassword(int id) {
		String query = " SELECT * FROM employeenew WHERE sap_id = ?";
		ResultSet resultSet;
		Employee employee = null;

		Connection connection = MySqlCon.getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			employee = new Employee();
			while (resultSet.next()) {

				employee.setSapId(resultSet.getInt("sap_id"));
				employee.setEmpName(resultSet.getString("emp_name"));

			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return employee;

	}

	// System.out.println("Rkeha");
}
