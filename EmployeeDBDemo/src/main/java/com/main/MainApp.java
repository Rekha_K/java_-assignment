package com.main;

import java.util.InputMismatchException;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.Exception.EmployeeException;
import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class MainApp {
	private static final Logger LOGGER = LogManager.getLogger(MainApp.class);

	public static void main(String[] args) {
		EmployeeService employeeService = new EmployeeServiceImpl();
		Employee employee = new Employee();
		int sapId = 0;
		String password = null;
		Scanner scanner = null;
		boolean error = false;
		LOGGER.info("Log message in Main method");
		do {
			try {
				scanner = new Scanner(System.in);
				System.out.print("Enter your SapId :");
				sapId = scanner.nextInt();
				error = false;
			} catch (InputMismatchException e) {
				System.err.println("Invalid Id");

				error = true;
			}
		} while (error);

		try {
			System.out.println("Enter your Password :");
			scanner = new Scanner(System.in);
			password = scanner.next();
			error = false;

		} catch (InputMismatchException e) {
			System.err.println("Invalid password");
			error = true;
		}

		try {
			if (password.length() == 5) {
				
				employee = employeeService.validateIdAndPassword(sapId, password, employee);
				if (employee.getEmpName() != null) {
					System.out.println("welcome " + employee.getEmpName() + " !");
					// LOGGER.info("welcome " + employee.getEmpName() + " !");
				} else {
					System.err.println("No such Employee found");
				}
			} else {
				System.err.println("Password length should be 5");
			}

		} catch (EmployeeException e) {

			// LOGGER.info(e.getMessage());
			System.err.println(e.getMessage());
		}
		scanner.close();
		employee = null;
		employeeService = null;
	}

}
