package com.model;

/**
 * 
 * @author rekha.k
 *
 */
public class Employee {
	private int sapId;
	private String empName;
	private String password;

	public Employee() {
		super();

	}

	public int getSapId() {
		return sapId;
	}

	public void setSapId(int sapId) {
		this.sapId = sapId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
