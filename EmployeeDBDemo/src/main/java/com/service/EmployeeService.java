package com.service;

import com.Exception.EmployeeException;
import com.model.Employee;
/**
 * 
 * @author rekha.k
 *
 */
public interface EmployeeService {
	public abstract Employee validateIdAndPassword(int empId, String password, Employee employee)throws EmployeeException;

}
