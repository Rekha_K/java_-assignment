package com.service;

import com.Exception.EmployeeException;
import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;
import com.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */
public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee validateIdAndPassword(int sapId, String password, Employee employee) throws EmployeeException {
		Employee employeeNow = null;
		EmployeeDao dao = new EmployeeDaoImpl();
		int empId = String.valueOf(sapId).length();

		if ((empId == 6) && (password.length() == 5)) {

			employeeNow = dao.validateIdAndPassword(sapId);

		} else {
			throw new EmployeeException("Employee not found");
		}
		return employeeNow;
	}

}
