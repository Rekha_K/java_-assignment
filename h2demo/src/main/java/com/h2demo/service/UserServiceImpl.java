package com.h2demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.h2demo.model.User;
import com.h2demo.repository.UserRepository;
@Service
public class UserServiceImpl implements UserService {
	
	//Dependency Injection
	@Autowired
	private UserRepository userRepository = null;

	@Override
	@Transactional
	public User creatUser(User user) {
		User user2 = null;
		user2 = userRepository.save(user);
		return user2;
	}

	@Override
	@Transactional
	public User readByUserId(int userId) {
		User user = null;
		Optional<User> optional = userRepository.findById(userId);
		if (optional.isPresent()) {
			user = optional.get();
		}
		return user;
	}

	@Override
	@Transactional
	public List<User> readAllUsers() {
		
		return userRepository.findAll();
	}

}
