package com.h2demo.service;

import java.util.List;

import com.h2demo.model.User;

public interface UserService {

	public abstract User creatUser(User user);

	public abstract User readByUserId(int userId);

	public abstract List<User> readAllUsers();

}
