package com.h2demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.h2demo.model.User;
import com.h2demo.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {

	// Dependency Injection
	@Autowired
	private UserService userService;

	@PostMapping(value = "/create")
	public ResponseEntity<User> createUser(@RequestBody User user) {
		User user2 = userService.creatUser(user);
		ResponseEntity<User> responseEntity = null;
		responseEntity = new ResponseEntity<User>(user2, HttpStatus.CREATED);
		return responseEntity;

	}

	@GetMapping(value = "/readbyid/{userId}")
	public ResponseEntity<User> readUserById(@PathVariable int userId) {
		User user = userService.readByUserId(userId);
		ResponseEntity<User> responseEntity = null;
		responseEntity = new ResponseEntity<User>(user, HttpStatus.OK);
		return responseEntity;

	}

	@GetMapping(value = "/readall")
	public ResponseEntity<List<User>> readAllUsers() {
		List<User> users = userService.readAllUsers();
		ResponseEntity<List<User>> responseEntity = null;
		responseEntity = new ResponseEntity<List<User>>(users, HttpStatus.OK);
		return responseEntity;

	}

}
