package com.producerservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author rekha.k
 *
 */
@AllArgsConstructor // lombok
@NoArgsConstructor
@Getter
@Setter
// @Entity
public class Employee {

	private int empNo;
	private String empName;
	private float salary;
	private int deptId; // from Department is a consumer

}
