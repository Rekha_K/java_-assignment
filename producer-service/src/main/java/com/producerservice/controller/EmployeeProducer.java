package com.producerservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.producerservice.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */
@RestController
@RequestMapping(value = "employee")
public class EmployeeProducer {

	@GetMapping(value = "/hai")
	public ResponseEntity<String> sayHello() {
		String hello = "Hello from producer";
		ResponseEntity<String> responseEntity;
		responseEntity = new ResponseEntity<String>(hello, HttpStatus.OK);
		// return new ResponseEntity<>("Hello from producer", HttpStatus.OK);
		return responseEntity;
	}

	@PostMapping(value = "createEmployee")
	public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {

		System.out.println("Employee Name :" + employee.getEmpName());
		System.out.println("Employee Id :" + employee.getEmpNo());
		System.out.println("Employee Department :" + employee.getDeptId());
		System.out.println("Employee created successfully !");

		return new ResponseEntity<Employee>(employee, HttpStatus.CREATED);
	}

	@GetMapping(value = "readEmployee")
	public ResponseEntity<List<Employee>> readAllEmployee() {
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee(10, "Ten", 1010.10f, 1)); // from DAO
		employees.add(new Employee(20, "Twenty", 2020.20f, 2));
		employees.add(new Employee(30, "Thirty", 3030.30f, 1));
		employees.add(new Employee(40, "Fourty", 4040.40f, 2));
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}

	@GetMapping(value = "readEmployee/{deptId}")
	public ResponseEntity<List<Employee>> readEmployeeByDeptId(@PathVariable int deptId) {
		List<Employee> lisEmployees = null;
		if (deptId == 1) {
			List<Employee> employees = new ArrayList<Employee>();
			employees.add(new Employee(10, "Ten", 1010.10f, 1));
			employees.add(new Employee(30, "Thirty", 3030.30f, 1));
			lisEmployees = employees;
		} else if (deptId == 2) {
			List<Employee> employees = new ArrayList<Employee>();
			employees.add(new Employee(20, "Twenty", 2020.20f, 2));
			employees.add(new Employee(40, "Fourty", 4040.40f, 2));
			lisEmployees = employees;
		}

		return new ResponseEntity<List<Employee>>(lisEmployees, HttpStatus.OK);
	}

	// @PostMapping(value = "createEmployee")
	// public ResponseEntity<Employee> createEmployee(@RequestBody Employee
	// employee) {
	//
	// System.out.println("Employee Name :" + employee.getEmpName());
	// System.out.println("Employee Id :" + employee.getEmpNo());
	// System.out.println("Employee Department :" + employee.getDeptId());
	// return new ResponseEntity<Employee>(employee, HttpStatus.CREATED);
	// }
	@PutMapping(value = "/updateEmployee")
	public ResponseEntity<Employee> updateAddress(@RequestBody Employee employee) {

		ResponseEntity<Employee> responseEntity = null;
		if (employee != null) {
			System.out.println("Employee Name :" + employee.getEmpName());
			System.out.println("Employee Id :" + employee.getEmpNo());
			System.out.println("Employee Department :" + employee.getDeptId());
			System.out.println("Employee Updated successfully !");
			return new ResponseEntity<Employee>(employee, HttpStatus.OK);
		}
		return responseEntity;

	}

	@DeleteMapping(value = "/deleteEmployeeById/{alias}")
	public ResponseEntity<Integer> deleteByDoorNo(@PathVariable("alias") int empId) {

		int delete = 0;
		System.out.println("Deleted Employeed Id :" + empId);
		ResponseEntity<Integer> responseEntity = new ResponseEntity<Integer>(delete, HttpStatus.GONE);

		return responseEntity;

	}
}
