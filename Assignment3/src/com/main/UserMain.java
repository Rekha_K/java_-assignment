package com.main;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.model.UserInformation;
import com.service.UserImpl;
import com.service.InterfaceUserName;

/**
 * 
 * @author rekha.k
 *
 */

public class UserMain {

	public static void main(String[] args) {

		UserInformation userInfo = new UserInformation();
		InterfaceUserName nameService = new UserImpl();
		
		System.out.println("Enter your UserId :");
		Scanner scanner = new Scanner(System.in);
		int userId = scanner.nextInt();
		
		System.out.println("Enter your Password :");
		String passWord = scanner.next();
		
		userInfo.setUserId(userId);
		userInfo.setPassWord(passWord);
		userId = userInfo.getUserId();
		passWord = userInfo.getPassWord();
		try {
			String userName1 = nameService.validateUserIdAndPassword(userId, passWord);
			System.out.println("Entered UserId:" + userId);
			System.out.println(userName1);
		} catch (InputMismatchException e) {
			System.out.println(e);
		}
		userInfo = null;
		nameService = null;
		scanner.close();

	}

}
