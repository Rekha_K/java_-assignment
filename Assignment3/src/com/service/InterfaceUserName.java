package com.service;

/**
 * 
 * @author rekha.k
 *
 */
public interface InterfaceUserName {
	public String validateUserIdAndPassword(int userId, String userName);

}
