package com.hcl.pp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author rekha.k
 *
 */
@EnableFeignClients
@SpringBootApplication
@EnableSwagger2
@EnableHystrix
public class UserConsMicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserConsMicroApplication.class, args);
	}

}
