package com.hcl.pp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.User;
import com.hcl.pp.repository.UserRepository;
import com.hcl.pp.validator.UserValidator;

/**
 * 
 * @author rekha.k
 *
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
//	public User addUser(UserValidator userRequest) throws PetPeersException {
//		User userCreated = null;
//
//		if (!(userRequest.getPassword().equals(userRequest.getConfirmPassword()))) {
//			throw new PetPeersException("Passwords do not match");
//		} else {
//			User userExistsWithUserName = userRepository.findByUserName(userRequest.getUserName());
//			if (userExistsWithUserName != null) {
//				throw new PetPeersException("User Name already in use. Please select a different User Name");
//			} else {
//				try {
//					ModelMapper modelMapper = new ModelMapper();
//					User userRequested = modelMapper.map(userRequest, User.class);
//					userCreated = userRepository.save(userRequested);
//					if (userCreated != null) {
//						return userCreated;
//					} else {
//						throw new PetPeersException("User Not created");
//					}
//				} catch (PetPeersException e) {
//					logger.error("{}", e.getMessage());
//				}
//			}
//			return userCreated;
//		}
//
//	}

	@Override
	@Transactional
	public User addUser(UserValidator userValidator) throws UserException {

		ModelMapper modelMapper = new ModelMapper();
		User userRequested = modelMapper.map(userValidator, User.class);

		User userSaved = null;
		User user = userRepository.findByUserNameAndUserPassword(userValidator.getUserName(),
				userValidator.getUserPassword());

		if (user != null) {
			throw new UserException("User Name already in use. Please select a different User Name");
		} else {
			userSaved = userRepository.save(userRequested);
		}

		return userSaved;
	}

	@Override
	@Transactional
	public User updateUser(User user) {
		User repoUser = userRepository.findByUserName(user.getUserName());
		User userPassword = null;
		if (!(user.getUserPassword().equals(repoUser.getUserPassword()))) {
			userPassword.setUserPassword(user.getUserPassword());
		}
		User user2 = userRepository.save(userPassword);
		return user2;

	}

	@Override
	public List<User> listUsers() throws UserException {
		List<User> users = userRepository.findAll();
		if (users != null) {
			return users;
		} else {
			throw new UserException("No User Exists");
		}

	}

	@Override
	public User getUserById(Long userId) {
		Optional<User> optionalUser = null;
		User user = null;
		optionalUser = userRepository.findById(userId);
		if (optionalUser.isPresent()) {
			user = optionalUser.get();
		}
		return user;
	}

	@Override
	@Transactional
	public void removeUser(User user) {

		if (user != null) {
			userRepository.delete(user);
		}

	}

	@Override
	public User findByUserName(String userName) {
		User user = userRepository.findByUserName(userName);
		return user;
	}

	@Override
	@Transactional
	public User authenticateUser(String name, String password) {

		User repoUser = null;
		User getUser = new User();
		getUser.setUserName(name);
		getUser.setUserPassword(password);

		repoUser = userRepository.findByUserNameAndUserPassword(name, password);

		User newUser = repoUser;

		if (repoUser != null) {
			return newUser;
		} else {
			return getUser;
		}

	}

}
