package com.hcl.pp.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.PetDto;
/**
 * 
 * @author rekha.k
 *
 */
@FeignClient(name = "simple-Ref",url = "http://localhost:9095/pet") 
public interface UserConsumer {
	
	@GetMapping("/hai")
	public abstract ResponseEntity<String> check();
	@GetMapping(value = "/getallpets")
	public abstract List<PetDto> getPets() throws UserException;
	@GetMapping(value = "/getallpets")
	public abstract List<PetDto> getMyPets();

}
