package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.User;
import com.hcl.pp.validator.UserValidator;

/**
 * 
 * @author rekha.k
 *
 */
public interface UserService {

	public abstract User addUser(UserValidator userValidator) throws UserException;
	// public abstract User addUser(User user);

	public abstract User updateUser(User user);

	public abstract List<User> listUsers() throws UserException;

	public abstract User getUserById(Long userId);

	public abstract void removeUser(User user);

	public abstract User findByUserName(String userName);

	// public abstract Pet buyPet(Long petId, Long userId) throws UserException;

	// public abstract Set<Pet> getMyPets(Long userId);

	public User authenticateUser(String name, String password);

}
