package com.hcl.pp.validator;

import java.io.Serializable;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.hcl.pp.model.PetDto;

/**
 * 
 * @author rekha.k
 *
 */
public class UserValidator implements Serializable {

	private static final long serialVersionUID = 6663190996567312288L;
	@NotNull(message = "Please enter UserName :")
	@NotEmpty(message = "UserName can't be empty")
	private String userName;

	@NotNull(message = "Please enter Password :")
	@NotEmpty(message = "Password can't be empty")
	private String userPassword;

	@NotNull(message = "Please enter Confirm Password :")
	@NotEmpty(message = "Confirm Password can't be empty")
	private transient String confirmPassword;

	private transient Set<PetDto> pets;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}
