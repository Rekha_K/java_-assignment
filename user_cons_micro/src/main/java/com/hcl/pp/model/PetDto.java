package com.hcl.pp.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author rekha.k
 *
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor

public class PetDto implements Serializable {

	private static final long serialVersionUID = 5915548474243144780L;

	private Long id;

	// @Size(min = 4, max = 5, message = "mininmum 3 char and max 6 char")
	private String name;

	// @Column(name = "PET_AGE", length = 2, nullable = true)
	private Integer age;

	// @Size(min = 2, max = 2, message = "mininmum and max 2 char")
	private String place;

	// @JoinColumn(name = "PET_OWNERID")
	// @ManyToOne(fetch = FetchType.EAGER)
	private Long ownerId;

}
