package com.hcl.pp.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.PetDto;
import com.hcl.pp.model.User;
import com.hcl.pp.service.UserConsumer;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.UserValidator;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author rekha.k
 *
 */

@Slf4j
@RestController
@RequestMapping(value = "/user")
public class UserAppController {

	@Autowired
	private UserService userService;

	@Autowired //
	private UserConsumer userConsumer;

	@GetMapping(value = "/hai")
	@HystrixCommand(fallbackMethod = "sorry")
	public ResponseEntity<String> sayHello() {
		String hello = "Hello from User_Consumer";
		ResponseEntity<String> responseEntity = null;
		responseEntity = new ResponseEntity<String>(hello, HttpStatus.OK);
		// return new ResponseEntity<>("Hello from producer", HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/userconsumer")
//	@HystrixCommand(fallbackMethod = "sorry")
	public ResponseEntity<String> getFromProdPet() {

		// producer -end point of the port
		// fetch data from producer

		return userConsumer.check();

	}

	// addUser
	@PostMapping(value = "/user/adduser")
	public ResponseEntity<User> addUser(@Valid @RequestBody UserValidator userValidator) {

		log.info("UserAppController.addUser()");
		User userToReg = null;
		ResponseEntity<User> responseEntity = null;
		System.out.println("User Name:" + userValidator.getUserName());
		System.out.println("Password :" + userValidator.getUserPassword());
		System.out.println("ConfirmPassword :" + userValidator.getConfirmPassword());
		try {
			userToReg = userService.addUser(userValidator);
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}
		if (userToReg != null) {
			responseEntity = new ResponseEntity<User>(userToReg, HttpStatus.CREATED);
		} else {
			responseEntity = new ResponseEntity<User>(userToReg, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return responseEntity;

	}

	// loginUser
	@GetMapping(value = "/user/login/{name}/{password}/{confirmPassword}")
	public ResponseEntity<User> loginUser(@PathVariable String name, @PathVariable String password,
			@PathVariable String confirmPassword) throws UserException {

		log.info("UserAppController.loginUser()");
		UserValidator userValidator = new UserValidator();
		userValidator.setUserName(name);
		userValidator.setUserPassword(password);
		userValidator.setConfirmPassword(confirmPassword);

		ResponseEntity<User> responseEntity = null;
		// try {
		if (userValidator != null) {

			User getUser = null;
			List<PetDto> petDtos = null;
			User user = null;
			getUser = userService.authenticateUser(userValidator.getUserName(), userValidator.getUserPassword());
			if (getUser != null) {

				Long userId = getUser.getId();
				String userName = getUser.getUserName();
				String userPassword = getUser.getUserPassword();
				String userConfirmPassword = getUser.getConfirmPassword();
				try {
					petDtos = userConsumer.getPets();
					if (petDtos != null) {
						Set<PetDto> petSet = new HashSet<PetDto>(petDtos);// convert list into set
						user = new User(userId, userName, userPassword, userConfirmPassword, petSet);
					} else {
						throw new UserException("No Pet Details");
					}
				} catch (UserException e) {
					System.err.println(e.getMessage());
				}
				responseEntity = new ResponseEntity<User>(user, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
			}
		}

		return responseEntity;

	}

	// Logout
	@GetMapping("/user/logout")
	public ResponseEntity<String> logout() {

		log.info("UserAppController.logout()");
		ResponseEntity<String> responseEntity = null;
		responseEntity = new ResponseEntity<String>("redirect:user/login", HttpStatus.ACCEPTED);
		return responseEntity;

	}

	// MyPets
	@GetMapping(value = "/mypets/{userId}")
	@HystrixCommand(fallbackMethod = "sorry")
	public ResponseEntity<List<PetDto>> myPets(@PathVariable Long userId) {

		log.info("UserAppController.myPets()");
		ResponseEntity<List<PetDto>> responseEntity = null;
		User user = userService.getUserById(userId);
		List<PetDto> petDtos = userConsumer.getMyPets();
		List<PetDto> petList = new ArrayList<PetDto>();

		for (PetDto petDto : petDtos) {

			if (petDto.getOwnerId() == user.getId()) {

				petList.add(petDto);
				responseEntity = new ResponseEntity<List<PetDto>>(petList, HttpStatus.FOUND);

			} else {
				responseEntity = new ResponseEntity<List<PetDto>>(petList, HttpStatus.NOT_FOUND);
			}

		}

		return responseEntity;

	}

	// petHome
	@GetMapping("/pets")
	public ResponseEntity<List<PetDto>> petHome() {

		log.info("UserAppController.petHome()");
		ResponseEntity<List<PetDto>> responseEntity = null;
		List<PetDto> petsList;
		try {

			petsList = userConsumer.getPets();
			if (petsList != null) {
				responseEntity = new ResponseEntity<List<PetDto>>(petsList, HttpStatus.FOUND);
			} else {
				responseEntity = new ResponseEntity<List<PetDto>>(petsList, HttpStatus.NOT_FOUND);
			}
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}
		return responseEntity;

	}

	public ResponseEntity<String> sorry() {

		return new ResponseEntity<String>("Server down", HttpStatus.EXPECTATION_FAILED);
	}

}
