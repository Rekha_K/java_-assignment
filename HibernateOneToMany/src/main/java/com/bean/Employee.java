package com.bean;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author rekha.k
 *
 */
@Entity(name = "ForeignKeyAssoEntity")
@Table(name = "Employee", uniqueConstraints = { @UniqueConstraint(columnNames = "ID") })
public class Employee implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private int empId;
	@Column(name = "NAME", unique = false, nullable = false, length = 100)
	private String name;

	@Column(name = "SALARY", unique = false, nullable = false, length = 100)
	private float salary;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "EMPLOYEE_ID")
	private Set<Account> accounts;

	public Employee() {
		super();
	}

	public Employee(int empId, String name, float salary, Set<Account> accounts) {
		super();
		this.empId = empId;
		this.name = name;
		this.salary = salary;
		this.accounts = accounts;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

}
