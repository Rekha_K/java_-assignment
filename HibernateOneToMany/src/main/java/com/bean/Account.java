package com.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author rekha.k
 *
 */
@Entity(name = "ForeignKeyAssoAccountEntity")
@Table(name = "Account", uniqueConstraints = { @UniqueConstraint(columnNames = "ACCOUNT_ID") })
public class Account implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACCOUNT_ID", unique = true, nullable = false)
	private int accId;
	@Column(name = "ACC_NUMBER", unique = true, nullable = false, length = 100)
	private int accNo;
	@ManyToOne
	private Employee employee;

	public Account(int accId, int accNo, Employee employee) {
		super();
		this.accId = accId;
		this.accNo = accNo;
		this.employee = employee;
	}

	public Account() {
		super();
	}

	public int getAccId() {
		return accId;
	}

	public void setAccId(int accId) {
		this.accId = accId;
	}

	public int getAccNo() {
		return accNo;
	}

	public void setAccNo(int accNo) {
		this.accNo = accNo;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
