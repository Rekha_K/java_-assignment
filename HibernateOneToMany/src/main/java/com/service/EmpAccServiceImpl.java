package com.service;

import com.bean.Employee;
import com.dao.EmpAccDaoImpl;
import com.exception.EmployeeException;

/**
 * 
 * @author rekha.k
 *
 */
public class EmpAccServiceImpl implements EmpAccService {

	@Override
	public Employee createEmployee(Employee employee) throws EmployeeException {
		Employee employeeDummy = null;
		EmpAccDaoImpl accDaoImpl = new EmpAccDaoImpl();
		if (employee != null) {
			employeeDummy = accDaoImpl.createEmployee(employee);
			return employeeDummy;
		} else {
			throw new EmployeeException(" Oops... unable to add Employee Record");
		}

	}

}
