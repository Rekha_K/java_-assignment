package com.service;

import com.bean.Employee;
import com.exception.EmployeeException;

/**
 * 
 * @author rekha.k
 *
 */
public interface EmpAccService {

	public abstract Employee createEmployee(Employee employee) throws EmployeeException;
}
