package com.main;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;

import com.bean.Account;
import com.bean.Employee;
import com.exception.EmployeeException;
import com.service.EmpAccServiceImpl;
import com.util.HibernateUtil;
/**
 * 
 * @author rekha.k
 *
 */
public class MainApp {

	public static void main(String[] args) {

		// Add new Employee object
		Employee employee1 = new Employee();
		employee1.setName("Bose");
		employee1.setSalary(1111.11f);
		Employee employee2 = new Employee();
		employee2.setName("Jose");
		employee2.setSalary(2222.22f);

		Account account1 = new Account();
		account1.setAccNo(111);
		Account account2 = new Account();
		account2.setAccNo(222);
		Account account3 = new Account();
		account3.setAccNo(333);
		Account account4 = new Account();
		account4.setAccNo(444);

		Set<Account> accounts1 = new HashSet<Account>();
		accounts1.add(account1);
		accounts1.add(account2);

		Set<Account> accounts2 = new HashSet<Account>();
		accounts2.add(account3);
		accounts2.add(account4);

		employee1.setAccounts(accounts1);
		employee2.setAccounts(accounts2);
		
		EmpAccServiceImpl accServiceImpl = new EmpAccServiceImpl();
		try {
			Employee employeeOne = accServiceImpl.createEmployee(employee1);
			Employee employeeTwo = accServiceImpl.createEmployee(employee2);
			System.out.println("Employee " +employeeOne.getName()+" created  Successfully!");
			System.out.println("Employee " +employeeTwo.getName()+" created  Successfully!");
			
		} catch (EmployeeException e) {
			
			System.err.println(e.getMessage());
		}

	}

}
