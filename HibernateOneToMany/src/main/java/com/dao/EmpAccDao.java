package com.dao;

import com.bean.Employee;
/**
 * 
 * @author rekha.k
 *
 */
public interface EmpAccDao {
	
	public abstract Employee createEmployee(Employee employee);

}
