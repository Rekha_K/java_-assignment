package com.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bean.Employee;
import com.util.HibernateUtil;
/**
 * 
 * @author rekha.k
 *
 */
public class EmpAccDaoImpl implements EmpAccDao {
	Session session = HibernateUtil.getSessionFactory().getCurrentSession();

	@Override
	public Employee createEmployee(Employee employee) {
		Transaction transaction = session.beginTransaction();
		session.save(employee);
		transaction.commit();
		return employee;
	}

}
