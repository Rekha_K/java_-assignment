package com.comparableinterface;

import java.util.function.Function;

/**
 * 
 * @author rekha.k
 *
 */
public class FunctionInterfaceMain {

	public static void main(String[] args) {
		Function<Employee, String> function = (astr) -> {
			return "Hello " + astr.getEmpName();
		};
		String name = function.apply(new Employee(222, "Rekha", 2222.22f));
		System.out.println(name);

	}

}
