package com.comparableinterface;

/**
 * 
 * @author rekha.k
 *
 */
public class MainComparableInter {

	public static void main(String[] args) {
		
		Comparable<Employee> comparable = (Employee employee) -> {
			System.out.println("Inside");
			// return employee.getEmpNo();
			return (int) employee.getSalary() + 1000;
		};
		int salary = comparable.compareTo(new Employee(111, "Rekha", 1111.11f));
		System.out.println(salary);
	}

}
