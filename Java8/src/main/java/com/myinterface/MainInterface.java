package com.myinterface;

/**
 * 
 * @author rekha.k
 *
 */
public class MainInterface {
	
	public static void main(String[] args) {
		
		MyInterface myinterface = new MyInterfaceImpl();

		int sum = myinterface.add(4, 5);
		System.out.println("Sum of two numbers :" + sum);
		System.out.println("static final variable :" + MyInterface.PI);

		int diff = myinterface.sub(9, 4);
		System.out.println("Difference between two numbers :" + diff);

		System.out.println("Multiplication with static method :" + MyInterface.multi(2, 5));
	}
}
