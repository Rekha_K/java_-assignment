package com.myinterface;

/**
 * 
 * @author rekha.k
 *
 */
public interface MyInterface {
	// final variable
	public static final float PI = 3.1415f;

	// abstract method
	public abstract int add(int a, int b);

	// default method
	public default int sub(int a, int b) {

		return a - b;

	}

	// static method
	public static int multi(int a, int b) {

		return a * b;

	}
}
