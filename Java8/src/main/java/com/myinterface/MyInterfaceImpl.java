package com.myinterface;

/**
 * 
 * @author rekha.k
 *
 */
public class MyInterfaceImpl implements MyInterface {

	@Override
	public int add(int a, int b) {

		return a + b;
	}

}
