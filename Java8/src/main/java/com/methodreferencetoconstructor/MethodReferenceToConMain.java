package com.methodreferencetoconstructor;

/**
 * 
 * @author rekha.k
 *
 */
public class MethodReferenceToConMain {

	public static void main(String[] args) {
		
		MyFunctionalInterface functionalInterface = MethodReferenceToCon::new;
		functionalInterface.match("Happy that ");

	}

}
