package com.methodreferencetoconstructor;

/**
 * 
 * @author rekha.k
 *
 */
public interface MyFunctionalInterface {
	// return object
	public abstract MethodReferenceToCon match(String name);

}
