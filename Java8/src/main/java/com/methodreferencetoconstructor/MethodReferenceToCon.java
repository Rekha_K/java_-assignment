package com.methodreferencetoconstructor;

/**
 * 
 * @author rekha.k
 *
 */
public class MethodReferenceToCon {

	public MethodReferenceToCon(String name) {
		super();
		System.out.println(name + "Parameter constructor is called");
	}

}
