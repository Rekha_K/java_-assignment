package com.methodreferencetoinstance;

/**
 * 
 * @author rekha.k
 *
 */
public class MethodReferenceToInstance {

	public void display() {
		
		System.out.println("Method Referrence to instance is called");
	}
}
