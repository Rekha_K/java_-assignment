package com.methodreferencetoinstance;
/**
 * 
 * @author rekha.k
 *
 */
public class MethodReferenceToInstanceMain {
	
	public static void main(String[] args) {
		
		MethodReferenceToInstance instance = new MethodReferenceToInstance();
		MyFunctInterface functInterface1 = () -> {
			System.out.println("Method Reference with Lambda Expression");
		};
		functInterface1.display();
		MyFunctInterface functInterface2 = instance::display;// Method Reference
		functInterface2.display();

	}

}
