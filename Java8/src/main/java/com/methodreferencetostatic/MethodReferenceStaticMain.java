package com.methodreferencetostatic;

/**
 * 
 * @author rekha.k
 *
 */
public class MethodReferenceStaticMain {

	public static void main(String[] args) {

		MyFuntionalInterface funtionalInterface = (num1, num2) -> {
			return num1 + num2;
		};

		System.out.println("with lambda :" + funtionalInterface.add(4, 5));

		MyFuntionalInterface funtionalInterface2 = MethodReferenceStatic::addTwoNumbers;
		int sum = funtionalInterface2.add(8, 9);
		System.out.println("Method Reference to static method :" + sum);
	}
}
