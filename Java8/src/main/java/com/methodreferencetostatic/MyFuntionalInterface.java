package com.methodreferencetostatic;

/**
 * 
 * @author rekha.k
 *
 */
@FunctionalInterface
public interface MyFuntionalInterface {

	public abstract int add(int num1, int num2);
}
