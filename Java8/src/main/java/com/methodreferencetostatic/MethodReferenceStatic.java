package com.methodreferencetostatic;

/**
 * 
 * @author rekha.k
 *
 */
public class MethodReferenceStatic {

	public static int addTwoNumbers(int num1, int num2) {
		
		return num1 + num2;

	}
}
