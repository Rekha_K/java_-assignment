package com.streamapi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @author rekha.k
 *
 */
public class EmployeeStreamMain {

	public static void main(String[] args) {
		Employee employee1 = new Employee(111, "Rekha", 1111.11f, "A");
		Employee employee2 = new Employee(222, "Mouni", 2222.22f, "A");
		Employee employee3 = new Employee(333, "Sayali", 3333.33f, "B");
		Employee employee4 = new Employee(444, "Sindu", 4444.44f, "D");

		List<Employee> employeesList = new ArrayList<Employee>();
		employeesList.add(employee1);
		employeesList.add(employee2);
		employeesList.add(employee3);
		employeesList.add(employee4);
		for (Employee employee : employeesList) {
			System.out.println("Employee Name :" + employee.getEmpName());
			System.out.println("Employee Band :" + employee.getBand());
			System.out.println("Employee Salary :" + employee.getSalary());
		}

		long counts = employeesList.stream().count();// Terminal Operation

		System.out.println("count the number of records :" + counts);
		employeesList.stream().forEach((band) -> {
			System.out.println("Employee Band :" + band.getBand());
		}); // Terminal Operation

		Long count = employeesList.stream().filter((band) -> band.getBand().equals("A")).count();
		System.out.println("How many Employees comes under band A :" + count);// Filter -Intermediate Operation

		Long nameCount = employeesList.stream().filter((name) -> name.getEmpName().startsWith("S")).count();
		System.out.println("Employee Name start with 'S':" + nameCount);

		boolean matched = employeesList.stream().anyMatch((name) -> name.getEmpName().startsWith("M"));

		System.out.println("Does Employee name starts with 'M':" + matched);// anyMatch - Terminal Operation

		List<Employee> employeeList = employeesList.stream().filter((name) -> name.getEmpName().startsWith("S"))
				.collect(Collectors.toList());
		long empCount = employeeList.stream().count();
		System.out.println("Employee count start with 'S':" + empCount);

		employeesList.stream().filter((name) -> name.getEmpName().startsWith("S")).forEach((name) -> {
			System.out.println("Employee name starts with 's' :" + name.getEmpName());
		});

		employeesList.stream().filter((salary) -> salary.getSalary() < 2500).forEach((name) -> {
			System.out.println("Employee getting salary higher than 2500:" + name.getEmpName());
			System.out.println("Employee Salary :" + name.getSalary());
		});

		employee1 = null;
		employee2 = null;
		employee3 = null;
		employee4 = null;
		employeesList = null;

	}

}
