package com.biconsumer;

import java.util.function.BiConsumer;

/**
 * 
 * @author rekha.k
 *
 */
public class BiConsumerMain {

	public static void main(String[] args) {
		BiConsumer<Integer, String> biConsumer = (a, b) -> {
			System.out.println("Interger Value :" + (a + 100));
			System.out.println("String Value :" + b + "\n" + "Length of String :" + b.length());
		};
		biConsumer.accept(10, "Welcome");

	}

}
