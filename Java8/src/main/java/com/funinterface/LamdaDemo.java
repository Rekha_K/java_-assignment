package com.funinterface;
/**
 * 
 * @author rekha.k
 *
 */
public class LamdaDemo {
	public static void main(String[] args) {
		
		 MyFunctInterface functInterface = () -> System.out.println("Welcome to Lamda");
		 functInterface.display();
	}
}
