package com.funinterface;

/**
 * 
 * @author rekha.k
 *
 */
@FunctionalInterface
public interface MyFunctInterface {

	public abstract void display();// SAM = Single Abstract Method

}
