package com.funinterface;

import java.util.ArrayList;
import java.util.List;

import com.comparableinterface.Employee;

/**
 * 
 * @author rekha.k
 *
 */
public class ForEachMain {

	public static void main(String[] args) {
		Employee employee1 = new Employee(111, "one", 1111.11f);
		Employee employee2 = new Employee(222, "Two", 2222.22f);
		Employee employee3 = new Employee(333, "Three", 3333.33f);

		List<Employee> employees = new ArrayList();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		// forEach that accepts single parameter and return no result
		employees.forEach((emp) -> {
			System.out.println("Employee name :" + emp.getEmpNo());
			System.out.println("Employee Name :" + emp.getEmpName());
			System.out.println("Employee Salary :" + emp.getSalary());
		});
		
		employee1 = null;
		employee2 = null;
		employee3 = null;
		employees = null;

	}

}
