package com.subtype;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author rekha.k
 *
 */
public class SubTypeDemo {

	public static void main(String[] args) {
		// super
		List<? super Number> list = new ArrayList();
		list.add(new Integer(10));
		list.add(new Float(10.9));

		for (Object object : list) {
			System.out.println(object);
		}
		List<Integer> list1 = Arrays.asList(4, 5, 6, 7);

		// printing the sum of elements in list
		System.out.println("Total sum is:" + sum(list1));

	}

	private static double sum(List<? extends Number> list) {// extends
		double sum = 0.0;
		for (Number i : list) {
			sum += i.doubleValue();
		}

		return sum;
	}

}
