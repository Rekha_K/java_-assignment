package com.consumerservice.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.consumerservice.model.Department;
import com.consumerservice.model.EmployeeDto;

/**
 * 
 * @author rekha.k
 *
 */
@RestController
@RequestMapping(value = "Department")
public class ConsumerController {

	@Autowired // inject RestTemplate from main method
	private RestTemplate restTemplate;

	@GetMapping(value = "/consumer")
	public ResponseEntity<String> getFromProducer() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		// producer -end point of the port
		// fetch data from producer
		String fromProducer = restTemplate
				.exchange("http://localhost:9091/employee/hai", HttpMethod.GET, entity, String.class).getBody();
		return new ResponseEntity<String>(fromProducer, HttpStatus.OK);

	}

	@PostMapping(value = "createDepartment")
	public ResponseEntity<Department> createEmployee(@RequestBody Department department) {

		System.out.println("Consumer :" + department.getDeptName());
		System.out.println("Consumer :" + department.getDeptNo());
		System.out.println("Consumer :" + department.getEmployeeDtos().get(0).getEmpName());
		return new ResponseEntity<Department>(department, HttpStatus.CREATED);
	}

	@GetMapping(value = "readDepartment")
	public ResponseEntity<List<Department>> readAllDepartment() {

		// along with Department =employee
		List<EmployeeDto> employeeDtos1 = restTemplate.getForObject("http://localhost:9091/employee/readEmployee/1",
				ArrayList.class);
		Department department1 = new Department(1, "Development", employeeDtos1);
		List<EmployeeDto> employeeDtos2 = restTemplate.getForObject("http://localhost:9091/employee/readEmployee/2",
				ArrayList.class);

		Department department2 = new Department(2, "Testing", employeeDtos2);
		List<Department> departments = new ArrayList<>();
		departments.add(department1);
		departments.add(department2);

		return new ResponseEntity<List<Department>>(departments, HttpStatus.OK);

	}

	@GetMapping(value = "readDeparmentById/{deptId}")
	public ResponseEntity<Department> readDepartmentDeptId(@PathVariable int deptId) {

		List<EmployeeDto> employeeDtos = restTemplate
				.getForObject("http://localhost:9091/employee/readEmployee/" + deptId, ArrayList.class);
		Department department = new Department(2, "Training", employeeDtos);
		return new ResponseEntity<Department>(department, HttpStatus.OK);
	}

	@PutMapping(value = "updateDepartment")
	public ResponseEntity<Department> updateEmployee(@RequestBody Department department) {

		System.out.println("Consumer DepartmentName :" + department.getDeptName());
		System.out.println("Consumer Department No :" + department.getDeptNo());
		System.out.println("Consumer Employees for Department :" + department.getEmployeeDtos().get(0).getEmpName());
		return new ResponseEntity<Department>(department, HttpStatus.OK);
	}
}
