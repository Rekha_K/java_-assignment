package com.consumerservice.model;
//@Entity // has a relationship

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author rekha.k
 *
 */
@AllArgsConstructor // lombok
@NoArgsConstructor
@Getter
@Setter
// @Entity Has-A relation
public class Department {

	private int deptNo;
	private String deptName;
	private List<EmployeeDto> employeeDtos;

}
