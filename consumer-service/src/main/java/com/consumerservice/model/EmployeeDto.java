package com.consumerservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author rekha.k
 *
 */
@AllArgsConstructor // lombok
@NoArgsConstructor
@Getter
@Setter
public class EmployeeDto {// DTO = Data transfer Object
	
	private int empNo;
	private String empName;
	private float salary;
	private int deptId;

}
