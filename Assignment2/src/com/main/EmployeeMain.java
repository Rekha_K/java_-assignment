package com.main;

import com.model.Account;
import com.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */
public class EmployeeMain {
	public static void main(String[] args) {
		Employee employee = new Employee();
		employee.setEmpId(101);
		employee.setEmpName("Rekha");

		Account savingAccount = new Account(323232);
		Account salaryAccount = new Account(424242);
		Account debitAccount = new Account(525252);
		Account[] accounts = new Account[3];
		accounts[0] = savingAccount;
		accounts[1] = salaryAccount;
		accounts[2] = debitAccount;
		employee.setAccount(accounts);
		
		Account[] accounts2 = employee.getAccount();
		System.out.println("Employee Id :" + employee.getEmpId() + "\n" + "Employee Name :" + employee.getEmpName());
		System.out.println(employee.getEmpName() + " has three Account numbers: ");
		for (int i = 0; i < accounts2.length; i++) {
			System.out.println( + accounts[i].getAccNo() );
		}

		// System.out.println("Savings Account :" + accounts[0].getAccNo());
		// System.out.println("Salary Account :" + accounts[1].getAccNo());
		// System.out.println("Debit Account :" + accounts[2].getAccNo());

		System.out.println("in SBI Bank");
	}
}
