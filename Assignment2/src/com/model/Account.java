package com.model;

/**
 * 
 * @author rekha.k
 *
 */
public class Account {
	private int accNo;

	public Account(int accNo) {
		this.accNo = accNo;

	}

	public int getAccNo() {
		return accNo;
	}

	public void setAccNo(int accNo) {
		this.accNo = accNo;
	}

}
