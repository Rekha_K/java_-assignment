package com.model;

/**
 * 
 * @author rekha.k
 *
 */
public class Department {

	private String deptName;

	private Employee1[] employee;

	public Department(String deptName, Employee1[] employee) {
		super();
		this.deptName = deptName;
		this.employee = employee;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Employee1[] getEmployee() {
		return employee;
	}

	public void setEmployee(Employee1[] employee) {
		this.employee = employee;
	}

}
