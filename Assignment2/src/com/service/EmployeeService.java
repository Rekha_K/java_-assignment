package com.service;

import com.model.Address;
import com.model.Department;
import com.model.Employee1;

/**
 * 
 * @author rekha.k
 *
 */
public class EmployeeService {

	public Employee1[] displayDeveloper(Department devDept) {

		Employee1[] devArray = null;

		if (devDept.getDeptName().equals("Developing")) {
			devArray = devDept.getEmployee();
			for (int i = 0; i < devArray.length; i++) {
				devArray[i].getEmpName();
			}
		}
		return devArray;

	}

	public Address[] displayDevAddress(Employee1[] emp) {
		Address[] address = null;
		for (int i = 0; i < emp.length; i++) {
			if (emp[i].getEmpName().equalsIgnoreCase("Rekha")) {
				address = emp[i].getAddress();

			}
		}

		return address;

	}

	public Address[] displayTestAddress(Employee1[] emp) {
		Address[] address = null;
		for (int i = 0; i < emp.length; i++) {
			if (emp[i].getEmpName().equalsIgnoreCase("Dany")) {
				address = emp[i].getAddress();

			} else {
				emp[i].getEmpName();
			}
		}

		return address;

	}

	public Employee1[] displayTester(Department tesDept) {

		Employee1[] tesArray = tesDept.getEmployee();

		if (tesDept.getDeptName().equals("Testing")) {
			for (int i = 0; i < tesArray.length; i++) {
				tesArray[i].getEmpName();
			}
			return tesArray;
		} else {
			return null;
		}

	}

}
