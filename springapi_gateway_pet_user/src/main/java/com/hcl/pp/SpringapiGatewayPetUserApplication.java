package com.hcl.pp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
@EnableEurekaClient
@SpringBootApplication
public class SpringapiGatewayPetUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringapiGatewayPetUserApplication.class, args);
	}

}
