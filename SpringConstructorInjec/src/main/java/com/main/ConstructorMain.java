package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Employee;

public class ConstructorMain {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/SpringConfig.xml");

		Employee employee = (Employee) applicationContext.getBean("employeeBean");

		System.out.println("Spring Constructor Injection");

		System.out.println("Employee Id :" + employee.getEmpId());
		System.out.println("Employee Name :" + employee.getEmpName());
		System.out.println("Employee Salary :" + employee.getEmpSalary());

	}

}
