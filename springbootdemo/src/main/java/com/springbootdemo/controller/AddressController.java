package com.springbootdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springbootdemo.model.Address;
import com.springbootdemo.service.AddressService;

/**
 * 
 * @author rekha.k
 *
 */
@RestController
@RequestMapping(value = "addressmain") // class level
public class AddressController {

	@Autowired
	private AddressService addressService = null;

	@GetMapping(value = "/") // end point -method level
	public String sayHello() {
		return "welcome to Spring Boot !";
	}

	@PostMapping(value = "/create")
	public ResponseEntity<Address> createAddress(@RequestBody Address address) {
		Address addToStore = null;
		addToStore = addressService.addAddress(address);
		ResponseEntity<Address> responseEntity = new ResponseEntity<Address>(addToStore, HttpStatus.CREATED);
		return responseEntity;

	}

	@GetMapping(value = "/readbyno/{doorNo}")
	public ResponseEntity<Address> readAddressByDoorNo(@PathVariable int doorNo) {

		Address address = null;
		ResponseEntity<Address> responseEntity = null;
		if (doorNo > 0) {
			address = addressService.readAddressByDoorNo(doorNo);
			responseEntity = new ResponseEntity<Address>(address, HttpStatus.OK);
		}
		return responseEntity;

	}

	@PutMapping(value = "/update")
	public ResponseEntity<Address> updateAddress(@RequestBody Address address) {

		Address addToUpdate = null;
		ResponseEntity<Address> responseEntity = null;
		if (address != null) {
			addToUpdate = addressService.alterAddress(address);
			responseEntity = new ResponseEntity<Address>(addToUpdate, HttpStatus.OK);
		}
		return responseEntity;

	}

	@DeleteMapping(value = "/delete/{alias}")
	public int deleteByDoorNo(@PathVariable("alias") int doorNo) {

		int delete = 0;

		delete = (Integer) addressService.removeAddress(doorNo);
		// ResponseEntity<Integer> responseEntity = new ResponseEntity<Integer>(delete,
		// HttpStatus.GONE);

		return delete;

	}

	@GetMapping(value = "custom/{doorNo}")
	public ResponseEntity<String> customQuery(@PathVariable int doorNo) {

		ResponseEntity<String> responseEntity = null;

		String state = addressService.stateById(doorNo);
		responseEntity = new ResponseEntity<String>(state, HttpStatus.OK);
		return responseEntity;

	}

	@GetMapping(value = "native")
	public ResponseEntity<List<Address>> nativeQuery() {

		ResponseEntity<List<Address>> responseEntity = null;

		List<Address> addresses = addressService.getAddressNativeQuery();
		responseEntity = new ResponseEntity<List<Address>>(addresses, HttpStatus.OK);
		return responseEntity;

	}

}
