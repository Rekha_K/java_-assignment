package com.springbootdemo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springbootdemo.model.Address;
import com.springbootdemo.repository.AddressRepository;

/**
 * 
 * @author rekha.k
 *
 */
@Service
public class AddressServiceImpl implements AddressService {

	@Autowired // dependency Injection
	private AddressRepository addressRepository = null;

	@Override
	@Transactional
	public Address addAddress(Address address) {
		Address address2 = null;

		address2 = addressRepository.save(address);
		System.out.println("Door no in service Layer :" + address.getDoorNo());
		System.out.println("city in service Layer :" + address.getCity());
		System.out.println("State no in service Layer :" + address.getState());
		// address.setCity(address.getCity()+"processed");
		return address2;
	}

	@Override
	@Transactional
	public Address readAddressByDoorNo(int doorNo) {

		Optional<Address> optionalAddress = null;// to avoid null pointer exception
		Address address = null;
		// if(doorNo == 100) {
		// address = new Address(100, "Chennai", "TamilNadu");
		// }

		optionalAddress = addressRepository.findById(doorNo);
		if (optionalAddress.isPresent()) {
			address = optionalAddress.get();
		}
		return address;
	}

	@Override
	public Address readAddressByState(String state) {
		return null;
	}

	@Override
	@Transactional
	public Address alterAddress(Address address) {

		Address address2 = null;
		// System.out.println("Door no in service Layer :" + address.getDoorNo());
		// System.out.println("city in service Layer :" + address.getCity());
		// System.out.println("State no in service Layer :" + address.getState());
		// address.setCity(address.getCity() + "updated");
		address2 = addressRepository.save(address);
		System.out.println("updated DoorNo " + address2.getDoorNo());
		System.out.println("updated City :" + address2.getCity());
		System.out.println("updated  State :" + address2.getState());
		return address2;

	}

	@Override
	@Transactional
	public int removeAddress(int doorNo) {

		addressRepository.deleteById(doorNo);// pk should be already available in database
		System.out.println("Deleted DoorNo :" + doorNo);
		return doorNo;
	}

	@Override
	public String stateById(int doorNo) {

		String state = addressRepository.anyName(doorNo);
		System.out.println("State : " + state + " for DoorNo :" + doorNo);
		return state;
	}

	@Override
	public List<Address> getAddressNativeQuery() {

		List<Address> addresses = addressRepository.getAllAddresses();
		return addresses;
	}

}
