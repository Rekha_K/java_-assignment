package com.springbootdemo.service;

import java.util.List;

import com.springbootdemo.model.Address;

/**
 * 
 * @author rekha.k
 *
 */
public interface AddressService {

	public abstract Address addAddress(Address address);

	public abstract Address readAddressByDoorNo(int doorNo);

	public abstract Address readAddressByState(String state);

	public abstract Address alterAddress(Address address);

	public abstract int removeAddress(int doorNo);

	public abstract String stateById(int doorNo);

	public abstract List<Address> getAddressNativeQuery();

}
