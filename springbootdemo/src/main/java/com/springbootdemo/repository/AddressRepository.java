package com.springbootdemo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springbootdemo.model.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {

	@Query("SELECT add.state from Address as add where add.doorNo = :id") // ORM== Hibernate==named Query
	public abstract String anyName(@Param("id") Integer id); // custom query

	@Query(value = "SELECT * from address", nativeQuery = true) // sql Query
	public abstract List<Address> getAllAddresses();

	

}
