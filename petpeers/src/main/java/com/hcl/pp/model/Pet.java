package com.hcl.pp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author rekha.k
 *
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PETS")
public class Pet implements Serializable {

	private static final long serialVersionUID = 5915548474243144780L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", length = 5, columnDefinition = "int")
	private Long id;

	@Column(name = "PET_NAME", length = 55)
	@Size(min = 4, max = 5, message = "mininmum 3 char and max 6 char")
	private String name;

	@Column(name = "PET_AGE", length = 2, nullable = true)
	private Integer age;
	@Column(name = "PET_PLACE", length = 55)
	@Size(min = 2, max = 2, message = "mininmum and max 2 char")
	private String place;

	@JoinColumn(name = "PET_OWNERID")
	@ManyToOne(fetch = FetchType.EAGER)
	private User owner;

}
