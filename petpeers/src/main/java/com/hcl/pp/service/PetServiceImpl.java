package com.hcl.pp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.model.Pet;
import com.hcl.pp.repository.PetRepository;

/**
 * 
 * @author rekha.k
 *
 */
@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepository petRepository;

	@Override
	@Transactional
	public List<Pet> getAllPets() {
		List<Pet> pets;
		List<Pet> pets2 = new ArrayList<Pet>();
		List<Pet> pets3 = new ArrayList<Pet>();
		pets = petRepository.getAllPets();
		for (Pet pet : pets) {

			if (pet.getOwner() != null) {

				pets2.add(pet);
			} else {
				pets3.add(pet);
			}
		}
		return pets3;
	}

	@Override
	@Transactional
	public Pet savePet(Pet pet) {

		Pet pet2 = null;
		if (pet != null) {
			pet2 = petRepository.save(pet);
		}
		return pet2;
	}

	@Override
	@Transactional
	public Pet getPetById(Long id) {
		Pet pet = null;
		Optional<Pet> petOptional = petRepository.findById(id);
		if(petOptional.isPresent()) {
			pet = petOptional.get();
		}
		return pet;
	}
}
