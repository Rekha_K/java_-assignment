package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.model.Pet;

/**
 * 
 * @author rekha.k
 *
 */
public interface PetService {

	public abstract Pet savePet(Pet pet);

	public abstract List<Pet> getAllPets();

	public abstract Pet getPetById(Long id);

}
