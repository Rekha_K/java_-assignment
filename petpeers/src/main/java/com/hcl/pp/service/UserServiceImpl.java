package com.hcl.pp.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.repository.UserRepository;

/**
 * 
 * @author rekha.k
 *
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PetRepository petRespository;

	@Override
	@Transactional
	public User addUser(User user1) {

		User user = new User();
		user.setUserName(user1.getUserName());
		user.setUserPassword(user1.getUserPassword());
		user.setConfirmPassword(user1.getConfirmPassword());
		User user2 = null;
		Set<Pet> petsList = new HashSet<>();
		if (user1.getPets() != null) {
			for (Pet pet : user1.getPets()) {
				Pet pp = new Pet();
				pp.setName(pet.getName());
				pp.setAge(pet.getAge());
				pp.setPlace(pet.getPlace());
				pp.setOwner(user);
				petsList.add(pp);
			}
			user.setPets(petsList);
			user2 = userRepository.save(user);
		} else {
			user2 = userRepository.save(user1);
		}

		return user2;
	}

	@Override
	@Transactional
	public User updateUser(User user) {

		User user2 = userRepository.save(user);
		return user2;
	}

	@Override
	@Transactional
	public List<User> listUsers() {

		List<User> users = userRepository.findAll();
		return users;
	}

	@Override
	@Transactional
	public User getUserById(Long userId) {

		User user = null;
		Optional<User> optional = userRepository.findById(userId);
		if (optional.isPresent()) {
			user = optional.get();
		}
		return user;
	}

	@Override
	@Transactional
	public void removeUser(User user) {

		userRepository.delete(user);

	}

	@Override
	@Transactional
	public User findByUserName(String userName) {

		User user = userRepository.findByUserName(userName);
		return user;
	}

	@Override
	@Transactional
	public User authenticateUser(String name, String password) {

		User repoUser = null;
		User getUser = new User();
		getUser.setUserName(name);
		getUser.setUserPassword(password);

		repoUser = userRepository.findByUserNameAndUserPassword(name, password);
		User newUser = repoUser;

		if (repoUser != null) {
			return newUser;
		} else {
			return getUser;
		}

	}

	@Override
	@Transactional
	public Pet buyPet(Long petId, Long ownerId) throws UserException {

		Optional<Pet> optionalPet =null;
		User user = new User();
		Pet pet = new Pet();
		optionalPet = petRespository.findById(petId);

		if (optionalPet.isPresent()) {
			if (optionalPet.get().getOwner() != null) {
				throw new UserException("Pet bought already by this owner,try with another pet");
			} else {
				Set<Pet> petList = new HashSet<Pet>();
				pet = optionalPet.get();
				petList.add(pet);
				user.setId(ownerId);
				pet.setOwner(user);
			}
		}
		petRespository.save(pet);
		return pet;
	}

	@Override
	@Transactional
	public Set<Pet> getMyPets(Long userId) {

		User owner = new User();
		Pet pet = new Pet();
		Set<Pet> pets = new HashSet<Pet>();
		Optional<User> optional = userRepository.findById(userId);
		if (optional.isPresent()) {
			owner = optional.get();
		}
		pet.setOwner(owner);
		pets.add(pet);

		return pets;
	}

}
