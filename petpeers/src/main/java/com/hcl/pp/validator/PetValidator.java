package com.hcl.pp.validator;

import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.Pet;

/**
 * 
 * @author rekha.k
 *
 */
public class PetValidator {

	public boolean petValidation(Pet pet) throws UserException {

		int petAge = pet.getAge();
		// pet.getId();
		String petName = pet.getName();

		String petPlace = pet.getPlace();
		if (petName == "" || petPlace == "" || petAge == 0) {
			throw new UserException("All fields are mandatory ");
		} else if (pet.getAge() < 0 || pet.getAge() > 99) {

			throw new UserException("Age should be in between 0-99");
		} else {
			return true;

		}

	}

}
