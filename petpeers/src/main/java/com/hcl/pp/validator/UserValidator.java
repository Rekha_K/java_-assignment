package com.hcl.pp.validator;

import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.User;

/**
 * 
 * @author rekha.k
 *
 */
public class UserValidator {

	public boolean validateUserLogin(User user) throws UserException {

		if (user.getUserName() == "" || user.getUserPassword() == "") {

			throw new UserException("All fields are mandatory");

		} else if (user.getUserName().equals(user.getUserPassword())) {

			throw new UserException("Username and Password must not be same");

		} else if (!(user.getUserPassword().equals(user.getConfirmPassword()))) {

			throw new UserException("Passwords do not match");

		} else {
			return true;
		}
	}

}
