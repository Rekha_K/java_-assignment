package com.hcl.pp.validator;

import java.io.Serializable;

import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.User;

/**
 * 
 * @author rekha.k
 *
 */

public class LoginValidator implements Serializable {

	private static final long serialVersionUID = -2034351612768556168L;

	// @NotEmpty(message = "Name may not be empty")
	// @Size(min = 2, max = 55, message = "Name must be between 2 and 55 characters
	// long")
	// private String userName;
	//
	// @NotEmpty(message = "Password may not be empty")
	// @Size(min = 2, max = 55, message = "password must be between 2 and 55
	// characters long")
	// private String userPassword;
	//
	// @NotEmpty(message = "confirmPassword may not be empty")
	// @Size(min = 2, max = 55, message = "password and confirmPassword Not
	// Matched")
	// private String confirmPassword;

	public boolean validateLoginUser(User getUser, User repoUser) throws UserException {

		String userName1 = getUser.getUserName();
		String userName2 = repoUser.getUserName();
		String confirmPassword = getUser.getConfirmPassword();
		String password = getUser.getUserPassword();
		

		if (getUser.getUserName() == "" || getUser.getUserPassword() == "") {

			throw new UserException("All fields are mandatory");
		} else if (userName1.equals(userName2)) {
			throw new UserException("User Name already in use. Please select a different User Name");
		} else if (!(password.equals(confirmPassword))) {
			throw new UserException("Passwords do not match");
		} else {
			return true;
		}

	}

	public boolean validateLogin(User getUser) throws UserException {

		String confirmPassword = getUser.getConfirmPassword();
		String password = getUser.getUserPassword();
		if (getUser.getUserName() == "" || getUser.getUserPassword() == "") {

			throw new UserException("All fields are mandatory");
		} else if (!(password.equals(confirmPassword))) {
			throw new UserException("Passwords do not match");
		} else {
			return true;
		}

	}
}
