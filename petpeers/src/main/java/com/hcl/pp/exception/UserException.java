package com.hcl.pp.exception;

/**
 * 
 * @author rekha.k
 *
 */
public class UserException extends Exception {

	private String message;

	public UserException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {

		return this.message;
	}

}
