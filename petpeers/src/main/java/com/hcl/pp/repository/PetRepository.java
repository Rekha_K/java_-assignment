package com.hcl.pp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hcl.pp.model.Pet;

/**
 * 
 * @author rekha.k
 *
 */
@Repository
public interface PetRepository extends JpaRepository<Pet, Long> {

	@Query(value = "SELECT * from pets", nativeQuery = true)
	public abstract List<Pet> getAllPets();
}
