package com.hcl.pp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.pp.model.User;

/**
 * 
 * @author rekha.k
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	// @Query("SELECT user from pet_user as user where user.user_name = :name") //
	// ORM== Hibernate==named Query
	// public abstract String findByName(@Param("name") String name);
	public abstract User findByUserName(String userName);

	public abstract User findByUserNameAndUserPassword(String name, String password);

}
