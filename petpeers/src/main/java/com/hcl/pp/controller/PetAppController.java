package com.hcl.pp.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.service.PetService;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.PetValidator;
import com.hcl.pp.validator.UserValidator;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author rekha.k
 *
 */
@Slf4j
@RestController
public class PetAppController {

	@Autowired
	private UserService userService;
	@Autowired
	private PetService petService;

	// addUser
	@PostMapping("/user/add")
	public ResponseEntity<User> addUser(@Valid @RequestBody User user) {

		log.info("PetsAppController.addUser()");
		ResponseEntity<User> responseEntity = null;
		LoginValidator loginValidator = new LoginValidator();

		String userName = user.getUserName();
		String password = user.getUserPassword();

		User addUser = null;
		boolean validate;

		User userAuthenticate = userService.authenticateUser(userName, password);

		try {
			if (userAuthenticate.getId() != null) {
				validate = loginValidator.validateLoginUser(user, userAuthenticate);

			} else {
				validate = loginValidator.validateLogin(user);
			}

			if (validate == true) {
				addUser = userService.addUser(user);
			} else {
				throw new UserException("Invalid Login Credentials");
			}

		} catch (UserException e) {
			System.err.println(e.getMessage());
		}
		System.out.println("addUer :" + addUser);
		if (addUser != null) {
			responseEntity = new ResponseEntity<User>(addUser, HttpStatus.CREATED);
		} else {
			responseEntity = new ResponseEntity<User>(addUser, HttpStatus.NO_CONTENT);
		}
		return responseEntity;

	}

	// loginUser
	@GetMapping(value = "/user/login")
	public ResponseEntity<User> loginUser(@Valid @RequestBody User user) throws UserException {

		log.info("PetsAppController.addUser()");
		ResponseEntity<User> responseEntity = null;
		try {
			if (user != null) {

				UserValidator userValidator = new UserValidator();
				boolean userValidation = userValidator.validateUserLogin(user);

				User getUser = null;
				if (userValidation == true) {

					getUser = userService.findByUserName(user.getUserName());
					if (getUser != null) {
						responseEntity = new ResponseEntity<User>(getUser, HttpStatus.OK);
					} else {
						responseEntity = new ResponseEntity<User>(getUser, HttpStatus.NOT_FOUND);
					}
				}

			}
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}
		return responseEntity;

	}
	//Logout
	@GetMapping("/user/logout")
	public ResponseEntity<String> logout() {

		log.info("PetsAppController.logout()");
		ResponseEntity<String> responseEntity = null;
		responseEntity = new ResponseEntity<String>("redirect:user/login", HttpStatus.ACCEPTED);
		return responseEntity;

	}

	// myPets
	@GetMapping("/pets/myPets/{userId}")
	public ResponseEntity<Set<Pet>> myPets(long userId) {

		log.info("PetsAppController.myPets()");
		ResponseEntity<Set<Pet>> responseEntity = null;
		Set<Pet> pets = userService.getMyPets(userId);
		if (pets != null) {
			responseEntity = new ResponseEntity<Set<Pet>>(pets, HttpStatus.OK);
		} else {
			responseEntity = new ResponseEntity<Set<Pet>>(pets, HttpStatus.NOT_FOUND);
		}
		return responseEntity;

	}

	// petHome
	@GetMapping("/pets")
	public ResponseEntity<List<Pet>> petHome() {

		log.info("PetsAppController.petHome()");
		ResponseEntity<List<Pet>> responseEntity = null;
		List<Pet> petsList = petService.getAllPets();
		if (petsList != null) {
			responseEntity = new ResponseEntity<List<Pet>>(petsList, HttpStatus.FOUND);
		} else {
			responseEntity = new ResponseEntity<List<Pet>>(petsList, HttpStatus.NOT_FOUND);
		}
		return responseEntity;

	}

	// petDetail
	@GetMapping("/pets/petDetail")
	public ResponseEntity<List<Pet>> petDetails() {

		log.info("PetsAppController.petDetails()");
		List<Pet> pet = petService.getAllPets();
		return new ResponseEntity<List<Pet>>(pet, HttpStatus.FOUND);
	}

	// addPet
	@PostMapping(value = "/pets/addPet")
	public ResponseEntity<Pet> addPet(@RequestBody Pet pet) {

		log.info("PetsAppController.addPet()");

		ResponseEntity<Pet> responseEntity = null;
		PetValidator petValidator = new PetValidator();
		boolean validate;
		Pet petAdded = null;

		try {
			validate = petValidator.petValidation(pet);
			if (validate == true) {
				petAdded = petService.savePet(pet);
			} else {
				throw new UserException("Entered Pet details are not valid");
			}
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}
		responseEntity = new ResponseEntity<Pet>(petAdded, HttpStatus.CREATED);
		return responseEntity;

	}

	// buyPet

	@PutMapping("/pets/buyPet/{petId}/{userId}")
	public ResponseEntity<Pet> buyPet(@PathVariable Long petId, @PathVariable Long userId) {

		log.info("PetsAppController.byPet()");
		ResponseEntity<Pet> responseEntity = null;
		Pet pet;
		try {
			pet = userService.buyPet(petId, userId);
			responseEntity = new ResponseEntity<Pet>(pet, HttpStatus.OK);
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}

		return responseEntity;
	}

	// @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	// @ExceptionHandler(DataIntegrityViolationException.class)
	// public Map<String, String> dataIntegrityViolationException(HttpServletRequest
	// req,
	// DataIntegrityViolationException ex) {
	// log.info("User Name is already used. please select a different User Name");
	// Map<String, String> errors = new HashMap<>();
	// errors.put("message", "User Name is already used. please select a different
	// User Name");
	// errors.put("path", req.getContextPath().toString());
	// return errors;
	// }

}
