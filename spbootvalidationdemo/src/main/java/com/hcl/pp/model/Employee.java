package com.hcl.pp.model;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * @author rekha.k
 *
 */
// POJO
public class Employee {

	private int empNo;
	@NotEmpty(message = "please enter Employee Name :")
	@Size(min = 3, max = 6, message = "mininmum 3 char and max 6 char")
	private String empName;
	private double salary;
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Past
	public LocalDate dob;
	@Email
	private String email;

	public Employee() {
		super();
	}

	public Employee(int empNo, String empName, double salary, LocalDate dob, String email) {
		
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
		this.dob = dob;
		this.email = email;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
