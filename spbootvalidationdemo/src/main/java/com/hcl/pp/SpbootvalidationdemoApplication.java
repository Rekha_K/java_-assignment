package com.hcl.pp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpbootvalidationdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbootvalidationdemoApplication.class, args);
	}

}
