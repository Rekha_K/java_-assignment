package com.hcl.pp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author rekha.k
 *
 */
@RestController
public class HelloController {

	@GetMapping(value = "add/{alias}")
	public int addTwoNumber(@PathVariable("alias") int num) {

		int sum = 0;
		if (num > 0) {
			sum = num + 10;
		} else {
			throw new ArrayIndexOutOfBoundsException();
		}
		return sum;

	}

}
