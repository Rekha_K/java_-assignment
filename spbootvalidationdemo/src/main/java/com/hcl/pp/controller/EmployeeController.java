package com.hcl.pp.controller;

import java.time.LocalDate;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */
@RestController
// @RequestMapping(value="employee")
public class EmployeeController {

	@PostMapping(value = "/create")
	public ResponseEntity<Employee> saveEmployee(@Valid @RequestBody Employee employee) {

		System.out.println("Employee no :" + employee.getEmpNo());
		System.out.println("Employee name :" + employee.getEmpName());
		System.out.println("Employee salary :" + employee.getSalary());
		System.out.println("Employee DOB :" + employee.getDob());
		System.out.println("Employee Mail Id :" + employee.getEmail());
		ResponseEntity<Employee> responseEntity = null;
		responseEntity = new ResponseEntity<Employee>(employee, HttpStatus.CREATED);
		return responseEntity;

	}

	@GetMapping("/")
	public Employee getEmployee() {

		return new Employee(100, "Rekha", 3333.33f, LocalDate.now(), "Rekha@gmail.com");

	}

	// @ExceptionHandler(MethodArgumentNotValidException.class)
	// public ResponseEntity<CustomError> handleException() {
	//// FieldError error = ex.getBindingResult().getFieldError();
	//// String defaultMessage = error.getDefaultMessage();
	// CustomError customError = new CustomError(LocalDateTime.now(),
	// defaultMessage, request.getDescription(false));
	// return new ResponseEntity(customError, HttpStatus.BAD_REQUEST);
	// }

}
