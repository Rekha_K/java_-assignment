package com.main;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import com.exception.UserException;
import com.model.Employee;
import com.service.EmpServiceImpl;
import com.service.EmpServiceInterface;

/**
 * 
 * @author rekha.k
 *
 */
public class EmployeeMain {
	public static void main(String[] args) {
		Employee employee1 = new Employee(1001, "Rekha", 6000.50f);
		Employee employee2 = new Employee(1002, "Mouni", 4000.50f);
		Employee employee3 = new Employee(1003, "Nithya", 5000.50f);
		Employee employee4 = new Employee(1004, "afafi", 7000.50f);

		System.out.println("Enter Employee Name :");
		Scanner scanner = new Scanner(System.in);
		String name = scanner.next();
		
		
		Set<Employee> employees = new HashSet<Employee>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		employees.add(employee4);
		EmpServiceInterface empServiceInterface = new EmpServiceImpl();
		Employee empName;
		try {
			empName = empServiceInterface.searchEmployeeName(employees, name);

			System.out.println("Employee Name Found:" + empName.getEmpName());
			System.out.println("Id for Employee:" + empName.getEmpId());
			System.out.println("Salary for Employee:" + empName.getSalary()+"\n");
		} catch (UserException e) {

			System.err.println(e.getMessage());
		}

		Employee empId;
		try {
			empId = empServiceInterface.searchEmployeeId(employees, employee4.getEmpId());
			System.out.println("Employee Id Found :" + empId.getEmpId());
			System.out.println("Name for Employee :" + empId.getEmpName());
			System.out.println("Salary for Employee :" + empId.getSalary()+"\n");
		} catch (UserException e) {

			System.err.println(e.getMessage());
		}
		float totalSalary = empServiceInterface.calculateSalary(employees);
		System.out.println("Total Salary for all Employees :" + totalSalary);
		employee1 = null;
		employee2 = null;
		employee3 = null;
		employee4 = null;
		employees = null;
		empServiceInterface = null;
		scanner.close();

	}
}
