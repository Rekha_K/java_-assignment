package com.service;

import java.util.Set;

import com.exception.UserException;
import com.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */
public class EmpServiceImpl implements EmpServiceInterface {

	public Employee searchEmployeeName(Set<Employee> employees, String name) throws UserException {

		Employee tempEmp = null;
		if (employees != null && name != null) {

			for (Employee employee : employees) {
				if (employee.getEmpName().equals(name)) {
					tempEmp = employee;

				}
			}
		} else {
			throw new UserException("Employee Name not Found");
		}

		return tempEmp;
	}

	public Employee searchEmployeeId(Set<Employee> employees, int id) throws UserException {
		Employee tempEmp = null;
		if (employees != null && id != 0) {
			for (Employee employee : employees) {
				if (employee.getEmpId() == id) {
					tempEmp = employee;

				}
			}
		} else {
			throw new UserException("Employee Id not Found");
		}

		return tempEmp;
	}

	public float calculateSalary(Set<Employee> employees) {
		int total = 0;
		for (Employee employee : employees) {
			total += employee.getSalary();

		}
		return total;
	}

}
