package com.service;

import java.util.Set;

import com.exception.UserException;
import com.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */
public interface EmpServiceInterface {
	
	Employee searchEmployeeName(Set<Employee> employees, String name) throws UserException;

	Employee searchEmployeeId(Set<Employee> employees, int id) throws UserException;

	float calculateSalary(Set<Employee> employees);
}
