package com.main;

import com.model.Calculator;

/**
 * 
 * @author rekha.k
 *
 */
public class ExceptionDemo {

	public static void main(String[] args) {
		
		Calculator calculator = new Calculator();
		int divAns = calculator.div(10, 5);
		System.out.println("Answer for Division :" + divAns);

		try {
			int divAns2 = calculator.div(10, 0);

			System.out.println("Answer for Division :" + divAns2);

		} catch (java.lang.ArithmeticException e) {
			System.err.println("Not a valid number");
		}

	}

}
