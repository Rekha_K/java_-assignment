package com.main;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author rekha.k
 *
 */
public class DateDemo {

	public static void main(String[] args) {
		Date todayDate = new Date();

		System.out.println("Today Date :" + todayDate);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println("Formatted Date :" + dateFormat.format(todayDate));

	}

}
