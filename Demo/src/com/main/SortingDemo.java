package com.main;

import java.util.Set;
import java.util.TreeSet;

import com.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */
public class SortingDemo {

	public static void main(String[] args) {
		Employee employee1 = new Employee(23, "REKHA", 8000f);
		Employee employee2 = new Employee(87, "MOUNI", 5675f);
		Employee employee3 = new Employee(98, "SAYALI", 7000f);
		Employee employee4 = new Employee(6, "LISWINI", 3000f);
		Set<Employee> employees = new TreeSet<>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		employees.add(employee4);
		for (Employee employee : employees) {
			System.out.println("Employee Name :" + employee.getEmpId());
			System.out.println("Employee Id :" + employee.getEmpName());
			System.out.println("Employee Salary :" + employee.getSalary());
		}
		employee1 = null;
		employee2 = null;
		employee3 = null;
		employee4 = null;
		employees = null;
	}

}
