package com.hcl.pp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.exception.PetException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.service.PetConsumer;
import com.hcl.pp.service.PetService;
import com.hcl.pp.validator.PetValidator;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author rekha.k
 *
 */
@Slf4j
@RestController
@RequestMapping(value = "/pet")
public class PetsAppController {
	@Autowired
	private PetService petService;
	@Autowired
	private PetConsumer petConsumer;

	@GetMapping(value = "/hai")
	public ResponseEntity<String> sayHello() {

		String hello = "Hello from Pet_Producer";
		ResponseEntity<String> responseEntity = null;
		responseEntity = new ResponseEntity<String>(hello, HttpStatus.OK);
		// return new ResponseEntity<>("Hello from producer", HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/petconsumer")
//	@HystrixCommand(fallbackMethod = "sorry")
	public ResponseEntity<String> getFromConsumerUser() {

		// producer -end point of the port
		// fetch data from producer

		return petConsumer.check();

	}

	// addPet
	@PostMapping(value = "/pets/addPet")
	public ResponseEntity<Pet> addPet(@Valid @RequestBody PetValidator petValidator) {

		log.info("PetsAppController.addPet()");
		ResponseEntity<Pet> responseEntity = null;
		Pet petAdded = null;
		try {
			petAdded = petService.savePet(petValidator);
		} catch (PetException e) {
			System.err.println(e.getMessage());
		}
		responseEntity = new ResponseEntity<Pet>(petAdded, HttpStatus.CREATED);
		return responseEntity;

	}

	// get AllPets
	@GetMapping(value = "/getallpets")
	public ResponseEntity<List<Pet>> getAllPets() {

		ResponseEntity<List<Pet>> responseEntity = null;
		List<Pet> petsList = null;
		try {
			petsList = petService.getAllPets();
		} catch (PetException e) {
			System.err.println(e.getMessage());
		}
		if (petsList != null) {
			responseEntity = new ResponseEntity<List<Pet>>(petsList, HttpStatus.OK);
		} else {
			responseEntity = new ResponseEntity<List<Pet>>(petsList, HttpStatus.NOT_FOUND);
		}

		return responseEntity;

	}

	// BuyPet
	@PutMapping(value = "/buypet/{userId}/{petId}")
	public ResponseEntity<Pet> buyPet(@PathVariable Long userId, @PathVariable Long petId) {
		ResponseEntity<Pet> responseEntity = null;
		Pet pet = null;
		try {
			pet = petService.getPetById(userId, petId);
		} catch (PetException e) {
			System.err.println(e.getMessage());
		}
		if (pet != null) {
			responseEntity = new ResponseEntity<Pet>(pet, HttpStatus.OK);
		} else {
			responseEntity = new ResponseEntity<Pet>(pet, HttpStatus.IM_USED);
		}

		return responseEntity;
	}

}
