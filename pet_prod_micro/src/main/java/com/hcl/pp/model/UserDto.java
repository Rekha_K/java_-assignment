package com.hcl.pp.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author rekha.k
 *
 */
//@Entity
//@Table(name = "PET_USER")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Serializable {

	private static final long serialVersionUID = -5934010972169898359L;
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "ID", unique = true, nullable = false)
	private Long id;

	@NotEmpty(message = "Please enter User Name :")
	@Size(min = 4, max = 6, message = "mininmum 4 char and max 6 char")
	// @Column(name = "USER_NAME", length = 55, unique = true)
	private String userName;

	// @Column(name = "USER_PASSWD", length = 55)
	@NotEmpty(message = "Please enter Password :")
	@Size(min = 5, max = 10, message = "mininmum 5 char and max 10 char")
	private String userPassword;

	@Size(min = 5, max = 10, message = "mininmum 5 char and max 10 char")
	@Transient
	private String confirmPassword;

	// @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL,fetch =
	// FetchType.EAGER)
	// @OneToMany(mappedBy = "owner",cascade = CascadeType.ALL)
	@Transient
	private Set<Pet> pets;

}
