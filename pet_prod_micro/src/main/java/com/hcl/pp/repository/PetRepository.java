package com.hcl.pp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.pp.model.Pet;

/**
 * 
 * @author rekha.k
 *
 */
public interface PetRepository extends JpaRepository<Pet, Long> {

}
