package com.hcl.pp.exception;

/**
 * 
 * @author rekha.k
 *
 */
public class PetException extends Exception {

	private String message;

	public PetException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {

		return this.message;
	}

}
