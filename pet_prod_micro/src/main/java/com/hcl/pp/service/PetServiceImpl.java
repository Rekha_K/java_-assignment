package com.hcl.pp.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.pp.exception.PetException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.validator.PetValidator;

/**
 * 
 * @author rekha.k
 *
 */
@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepository petRepository;

	@Override
	@Transactional
	public Pet savePet(PetValidator petValidator) throws PetException {

		ModelMapper modelMapper = new ModelMapper();
		Pet petReq = modelMapper.map(petValidator, Pet.class);
		Pet savePet = null;
		if (petReq != null) {
			savePet = petRepository.save(petReq);
		} else {
			throw new PetException("No pets to Save");
		}

		return savePet;
	}

	@Override
	@Transactional
	public Pet savePet(Pet pet) {

		Pet savePet = null;
		if (pet != null) {
			savePet = petRepository.save(pet);
		}

		return savePet;
	}

	@Override
	public List<Pet> getAllPets() throws PetException {

		List<Pet> pets = petRepository.findAll();
		if (pets != null) {
			return pets;
		} else {
			throw new PetException("No Pets Available");
		}

	}

	@Transactional
	@Override
	public Pet getPetById(Long userId, Long petId) throws PetException {

		Optional<Pet> petOptional = petRepository.findById(petId);
		Pet petUpdate = null;

		if (petOptional.isPresent()) {
			Pet pet2 = petOptional.get();
			if (pet2.getOwnerId() != null) {
				throw new PetException("Pet bought already by this owner,try with another pet");
			} else {
				pet2.setOwnerId(userId);
				petUpdate = petRepository.save(pet2);
			}

		}
		return petUpdate;
	}

}
