package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.exception.PetException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.validator.PetValidator;

/**
 * 
 * @author rekha.k
 *
 */
public interface PetService {
	public abstract Pet savePet(PetValidator petValidator) throws PetException;

	public abstract Pet savePet(Pet pet);

	public abstract List<Pet> getAllPets() throws PetException;

	public abstract Pet getPetById(Long userId, Long petId) throws PetException;

}
