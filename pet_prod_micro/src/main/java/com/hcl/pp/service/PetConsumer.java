package com.hcl.pp.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 
 * @author rekha.k
 *
 */
@FeignClient(name = "simple-Ref", url = "http://localhost:9096/user")
public interface PetConsumer {
	@GetMapping("/hai")
	public abstract ResponseEntity<String> check();

}
