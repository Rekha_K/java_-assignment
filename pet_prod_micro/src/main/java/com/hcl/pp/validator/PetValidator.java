package com.hcl.pp.validator;

import java.io.Serializable;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author rekha.k
 *
 */
public class PetValidator implements Serializable {
	private static final long serialVersionUID = 8842211433788795263L;

	@NotNull(message = "Please enter PetName :")
	// @NotEmpty(message = "Pet Name can't be empty")
	private String name;

	@NotNull(message = "Please enter Pet Age :")
	@Digits(integer = 2, fraction = 0, message = "Age must be in Number ")
	@Min(value = 0, message = "Age must be minimum 0 years")
	@Max(value = 99, message = "Age must be maximum upto 99 years")
	private Integer age;

	@NotNull(message = "Please enter Place :")
	// @NotEmpty(message = "Place can't be empty")
	private String place;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

}
