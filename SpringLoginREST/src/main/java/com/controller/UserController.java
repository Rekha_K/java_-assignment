package com.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.User;
import com.service.UserImpl;
import com.service.UserInterface;

/**
 * 
 * @author rekha.k
 *
 */
@RestController
public class UserController {

	UserInterface userInterface = new UserImpl();

	// create
	@PostMapping(value = "create")
	public ResponseEntity<User> createUser(@RequestBody User user) {

		User userCreated = userInterface.createUser(user);
		System.out.println("User Id :" + userCreated.getUserId());
		System.out.println("User Name :" + userCreated.getUserName());
		System.out.println("User City :" + userCreated.getCity());
		System.out.println("User created Successfully!");
		ResponseEntity<User> responseEntity = new ResponseEntity<User>(userCreated, HttpStatus.OK);
		return responseEntity;

	}

	// read

	@GetMapping(value = "readbyid/{alias}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> readEmployeeId(@PathVariable("alias") int userId) {// to get only data from
		User user = null;
		user = userInterface.readUserById(userId);

		ResponseEntity<User> responseEntity = new ResponseEntity<User>(user, HttpStatus.FOUND);
		return responseEntity;

	}

	@PutMapping(value = "update/{changeName}")
	public ResponseEntity<User> updateEmployee(@RequestBody User user, @PathVariable String changeName) {
		User user2 = userInterface.updateUser(user);
		// user.setUserName(changeName);

		System.out.println("Employee Name :" + user2.getUserName() + " updated Successfully!");

		ResponseEntity<User> responseEntity = new ResponseEntity<User>(user, HttpStatus.OK);

		return responseEntity;

	}

	// delete

	@DeleteMapping(value = "delete/{userId}")
	public ResponseEntity<Boolean> deleteUserById(@PathVariable int userId) {
		Boolean flag = userInterface.deleteUserById(userId);
		System.out.println("Deleted User Id :" + userId);
		ResponseEntity<Boolean> responseEntity = new ResponseEntity<Boolean>(flag, HttpStatus.GONE);
		return responseEntity;

	}

}
