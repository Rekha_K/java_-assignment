package com.service;

import com.model.User;

/**
 * 
 * @author rekha.k
 *
 */
public interface UserInterface {

	public User createUser(User user);

	public User readUserById(int userId);

	public User updateUser(User user);

	public boolean deleteUserById(int userId);

}
