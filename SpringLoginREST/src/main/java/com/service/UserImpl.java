package com.service;

import java.util.ArrayList;
import java.util.List;

import com.model.User;

/**
 * 
 * @author rekha.k
 *
 */
public class UserImpl implements UserInterface {

	@Override
	public User createUser(User user) {
		if (user.getUserId() != 0) {
			return user;
		} else {
			return null;

		}
	}

	@Override
	public User readUserById(int userId) {
		User user = new User();
		User userNew = null;
		user.setUserId(userId);
		if (userId != 0) {
			User user1 = new User(111, "Valan", "Chennai");
			User user2 = new User(222, "Arasu", "Bangalore");
			User user3 = new User(333, "Jerald", "Madurai");
			List<User> userList = new ArrayList<User>();
			userList.add(user1);
			userList.add(user2);
			userList.add(user3);

			for (User oneUser : userList) {
				if (oneUser.getUserId() == user.getUserId()) {
					userNew = oneUser;

					return userNew;
				}
				/*
				 * } else { return null; }
				 */
			}

		}
		return userNew;
	}

	@Override
	public User updateUser(User user) {
		if (user != null) {
			return user;
		} else {
			return null;
		}

	}

	@Override
	public boolean deleteUserById(int userId) {
		if (userId != 0) {
			return true;
		} else {
			return false;
		}

	}

}
