package com.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * 
 * @author rekha.k
 *
 */
public class MainApplication {
	private static final Logger LOGGER = LogManager.getLogger(MainApplication.class);

	public static void main(String[] args) {
		LOGGER.info("Log Main method");
	}

}
