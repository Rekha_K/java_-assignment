package com.main;

import com.model.Employee1;
import com.service.EmployeeService1;
/**
 * 
 * @author rekha.k
 *
 */
public class EmployeeMain1 {

	public static void main(String[] args) {
		Employee1 employee1 = new Employee1(21, "Mary", 2000);
		Employee1 employee2 = new Employee1(21, "Jhon", 3000);
		Employee1 employee3 = new Employee1(21, "Babu", 4000);
		Employee1 employee4 = new Employee1(21, "Dany", 5000);

		Employee1[] employees = new Employee1[4];// Collection of Objects

		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;
		employees[3] = employee4;
		EmployeeService1 employeeService1 = new EmployeeService1();
		
		float salary = employeeService1.sumSalary(employees);

		String name = employeeService1.findEmployee(employees);
		// System.out.println("Iteration");

		for (int i = 0; i < employees.length; i++) {
			System.out.println("Employee number : " + employees[i].getEmpId());
			System.out.println("Employee Name : " + employees[i].getEmpName());
			System.out.println("Employee salary : " + employees[i].getSalary());
			System.out.println( "\n");
		}
		System.out.println("Employee Name Found :" + name + "\n");
		System.out.println("Total salary for all the Employees :" + salary);
		employee1 = null;
		employee2 = null;
		employee3 = null;
		employee4 = null;
		employees = null;
		employeeService1 = null;
	}

}
