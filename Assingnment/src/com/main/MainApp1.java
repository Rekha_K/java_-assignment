package com.main;

import com.model.Employee1;
import com.service.EmployeeService;

/**
 * 
 * @author rekha.k
 *
 */
public class MainApp1 {

	public static void main(String[] args) {
		Employee1 employee1 = new Employee1(01, "Jhon", 3000f);
		Employee1 employee2 = new Employee1(02, "Ruban", 5000f);
		EmployeeService employeeService = new EmployeeService();
		Employee1 employeeOne = employeeService.higherSalary(employee1, employee2);
		
		System.out.println("Employee Id :" + employee1.getEmpId());
		System.out.println("Employee Name :" + employee1.getEmpName());
		System.out.println("Employee Salary :" + employee1.getSalary());
		System.out.println("Employee Id :" + employee2.getEmpId());
		System.out.println("Employee Name :" + employee2.getEmpName());
		System.out.println("Employee Salary :" + employee2.getSalary());
		System.out.println(employeeOne.getEmpName() + " have got Higher Salary than " + employee1.getEmpName());

	}

}
