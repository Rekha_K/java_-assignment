package com.main;

import com.service.ArraySum;
/**
 * 
 * @author rekha.k
 *
 */
public class ArraySumMain {

	public static void main(String[] args) {
		
		ArraySum arrayCreation = new ArraySum();

		int[] arr = { 10, 20, 30, 40, 50 };
		int intArray = arrayCreation.getArraySum(arr);
		System.out.print("The Sum of Array :");

		System.out.println(intArray);
		arrayCreation = null;
	}

}
