package com.main;

import com.service.Calculation;

/**
 * 
 * @author rekha.k
 *
 */
public class CalculationMain {

	public static void main(String[] args) {
		Calculation calculation = new Calculation();

		int add1 = calculation.add(3, 4);
		int add2 = calculation.add(5, 2, 6);

		double multiply = calculation.mutiply(2, 6.1);
		int divide = calculation.divide(12, 2);

		System.out.println("The Result of Addition1 :" + add1);
		System.out.println("The Result of Addition2:" + add2);

		// System.out.println("The Result of Subtraction is :"+sub);
		System.out.println("The Result of Multiplication  :" + multiply);
		System.out.println("The Result of Division:" + divide);
		calculation = null;
	}

}
