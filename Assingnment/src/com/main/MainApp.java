package com.main;

import com.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */

public class MainApp {

	public void display() {
		System.out.println("Welcome to Java!");
	}

	public static void main(String[] args) {

		Employee employee = new Employee();
		MainApp mainApp = new MainApp();
		mainApp.display();
		employee.setEmpId(51978394);
		employee.setEmpName("Rekha");
		employee.setSalary(5000f);
		
		System.out.println("Employee Id :" + employee.getEmpId());
		System.out.println("Employee Name :" + employee.getEmpName());
		System.out.println("Employee Salary :" + employee.getSalary());
		employee = null;
		mainApp = null;

	}

}
