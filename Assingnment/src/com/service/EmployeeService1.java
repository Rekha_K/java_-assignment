package com.service;

import com.model.Employee1;

public class EmployeeService1 {
	float sum;

	public float sumSalary(Employee1[] employee) {

		for (int i = 0; i < employee.length; i++) {
			sum += employee[i].getSalary();

		}
		
		return sum;

	}

	public String findEmployee(Employee1[] employees) {
		String name = null;
		if (employees != null) {
			for (int i = 0; i < employees.length; i++) {
				if (employees[i].getEmpName().equals("Jhon")) {
					name = employees[i].getEmpName();
				}
			}
			return name;
		} else {
			return "Name not Found";
		}

	}

}
