package com.model;

public class Employee {
	private int empId;
	private String empName;
	private float salary;

	public Employee() {
		super();
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {

		return "Hello" + " " + empName + "!";
	}

	public void setEmpName(String empName) {
		this.empName = empName;

	}
}
