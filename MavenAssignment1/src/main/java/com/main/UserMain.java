package com.main;

import java.util.Scanner;

import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;
/**
 * 
 * @author rekha.k
 *
 */
public class UserMain {

	public static void main(String[] args) {
		System.out.println("Enter User id :");
		Scanner  scanner= new Scanner(System.in);
		int userId = scanner.nextInt();
		
		System.out.println("Enter Password :");
		String password = scanner.next();
		UserService service = new UserServiceImpl();
		User user = service.validateUserIdAndPassword(userId, password);
		
	if(user != null) {
		System.out.println("User ID :"+user.getUserId());
		System.out.println("User Name :"+user.getUserName());
		System.out.println("Welcome "+user.getUserName()+"!");
	} else {
		System.err.println("Please give valid Information !");
	}
	scanner.close();
	service = null;
	user = null;
	}

}
