package com.dao;

import com.model.User;
/**
 * 
 * @author rekha.k
 *
 */
public interface UserDao {
public abstract User validateUserIdAndPassword(int userId,String password);
}
