package com.dao;

import com.model.User;
/**
 * 
 * @author rekha.k
 *
 */
public class UserDaoImpl implements UserDao {

	@Override
	public User validateUserIdAndPassword(int userId, String password) {
		User user = null;
		if (userId == 123456 && password.equals("rekha2@")) {
			user = new User(userId, "Rekha", password);
		} else if (userId == 232323 && password.equals("Hello3#")) {
			user = new User(userId, "Mouni", password);
		}

		return user;
	}

}
