package com.service;

import com.model.User;
/**
 * 
 * @author rekha.k
 *
 */
public interface UserService {
	public abstract User validateUserIdAndPassword(int userId,String password);
}
