package com.service;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.model.User;

/**
 * 
 * @author rekha.k
 *
 */
public class UserServiceImpl implements UserService {

	@Override
	public User validateUserIdAndPassword(int userId, String password) {
		User user = null;
		int idLength = String.valueOf(userId).length();
		if (idLength > 5 && password.length() > 5) {
			UserDao dao = new UserDaoImpl();
			user = dao.validateUserIdAndPassword(userId, password);
		} else {
			return null;
		}

		return user;
	}

}
