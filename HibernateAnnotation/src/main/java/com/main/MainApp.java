package com.main;

import java.util.List;
import java.util.Scanner;

import com.bean.Employee;
import com.exception.EmployeeException;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

/**
 * 
 * @author rekha.k
 *
 */
public class MainApp {

	public static void main(String[] args) {

		EmployeeService employeeService = new EmployeeServiceImpl();
		Employee employee = null;
		System.out.println("Enter your choice :");
		Scanner scanner = new Scanner(System.in);
		String crud = scanner.next();
		switch (crud) {
		case ("Create"):

			employee = new Employee("Maria", 4500.45f);
			try {
				employeeService.createEmployee(employee);
			} catch (EmployeeException e) {
				// TODO Auto-generated catch block
				System.err.println(e.getMessage());
			}
			break;
		case ("Read"):

			try {
				employee = employeeService.readEmployeeId(3);
				System.out.println("Read Employee by Id");
				System.out.println("Employee Id :" + employee.getEmpId());
				System.out.println("Employee Name :" + employee.getEmpName());
				System.out.println("Employee Salary :" + employee.getSalary());
			} catch (EmployeeException e) {

				System.err.println(e.getMessage());
			}
			List<Employee> employees;
			try {
				employees = employeeService.readAllEmployees();
				System.out.println("Read all Employees");
				for (Employee employee1 : employees) {
					System.out.println("Employee Id :" + employee1.getEmpId());
					System.out.println("Employee Name :" + employee1.getEmpName());
					System.out.println("Employee Salary :" + employee1.getSalary());
				}
			} catch (EmployeeException e) {

				System.err.println(e.getMessage());
			}
			break;

		case ("Update"):
			Employee employee1 = new Employee();
			employee1.setEmpName("Jack");
			employee1.setSalary(2222.22f);
			employee1.setEmpId(4);
			try {
				Employee employee2 = employeeService.updateEmployee(employee1);
				System.out.println(employee2.getEmpName());
				System.out.println("Employee updated successfully !");
			} catch (EmployeeException e) {

				System.err.println(e.getMessage());
			}
			break;
		case ("Delete"):

			int number = employeeService.deleteEmployeeById(3);
			if (number == 1) {

				System.out.println("Employee Record Deleted Successfully!");
			} else {
				System.out.println("Not Deleted");
			}
			break;
		default:
			System.out.println("\"Enter Read,create,update or delete\";");
		}
		scanner.close();
		employeeService = null;

	}

}
