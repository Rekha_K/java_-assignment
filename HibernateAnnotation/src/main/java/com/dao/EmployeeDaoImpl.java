package com.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import com.bean.Employee;
import com.util.HibernateUtil;
/**
 * 
 * @author rekha.k
 *
 */
public class EmployeeDaoImpl implements EmployeeDao {
	
	Session session = HibernateUtil.getSessionFactory().getCurrentSession();

	public Employee createEmployee(Employee employee) {

		Transaction transaction = session.beginTransaction();
		session.save(employee);
		transaction.commit();
		return employee;
	}

	public List<Employee> readAllEmployees() {
		List<Employee> employeeList = null;

		Transaction transaction = session.beginTransaction();
		/*
		 * String queryStr = "select emp from Employee emp"; Query query =
		 * session.createQuery(queryStr); employeeList = query.list();
		 */
		try {
			String readAllEmployees = "From Employee";
			employeeList = session.createQuery(readAllEmployees).list();
			transaction.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			// handle exception here
		} finally {
			try {
				if (session != null)
					session.close();
			} catch (Exception ex) {
			}
		}

		return employeeList;
	}

	public Employee readEmployeeId(int empId) {
		
		Employee employee = null;
		try {

			Transaction transaction = session.beginTransaction();
			// String queryStr = "select emp from Employee emp";

			employee = session.get(Employee.class, empId);
			transaction.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			// handle exception here
		} finally {
			try {
				if (session != null)
					session.close();
			} catch (Exception ex) {
			}
		}
		return employee;
	}

	public Employee updateEmployee(Employee employee) {

		Transaction transaction = session.beginTransaction();
		session.update(employee);
		transaction.commit();
		return employee;
	}

	public int deleteEmployeeById(int empId) {

		Transaction transaction = session.beginTransaction();
		session.delete(session.get(Employee.class, empId));
		transaction.commit();
		return 1;
	}

}
