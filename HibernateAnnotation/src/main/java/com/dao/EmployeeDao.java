package com.dao;

import java.util.List;

import com.bean.Employee;
/**
 * 
 * @author rekha.k
 *
 */
public interface EmployeeDao  {
	public abstract Employee createEmployee(Employee employee);

	public abstract List<Employee> readAllEmployees();

	public abstract Employee readEmployeeId(int empId);

	public abstract Employee updateEmployee(Employee employee);

	public abstract int deleteEmployeeById(int empId);


}
