package com.service;

import java.util.List;

import com.bean.Employee;
import com.exception.EmployeeException;
/**
 * 
 * @author rekha.k
 *
 */
public interface EmployeeService {
	
	public abstract Employee createEmployee(Employee employee) throws EmployeeException;

	public abstract List<Employee> readAllEmployees() throws EmployeeException;

	public abstract Employee readEmployeeId(int empId)throws EmployeeException;

	public abstract Employee updateEmployee(Employee employee) throws EmployeeException;

	public abstract int deleteEmployeeById(int empId);


}
