package com.service;

import java.util.List;

import com.bean.Employee;
import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;
import com.exception.EmployeeException;
/**
 * 
 * @author rekha.k
 *
 */
public class EmployeeServiceImpl implements EmployeeService {
	EmployeeDao employeeDao = new EmployeeDaoImpl();

	public Employee createEmployee(Employee employee) throws EmployeeException {

		if (employee != null) {

			employeeDao.createEmployee(employee);
			return employee;
		} else {
			throw new EmployeeException(" Oops... unable to add Employee Record");
		}

	}

	public List<Employee> readAllEmployees() throws EmployeeException {

		List<Employee> employees = employeeDao.readAllEmployees();
		if (employees != null) {
			return employees;
		} else {
			throw new EmployeeException("Employee not found");
		}

	}

	public Employee readEmployeeId(int empId) throws EmployeeException {
		Employee employee = null;

		if (empId != 0) {
			if(String.valueOf(empId).matches("^[0-9]*$")) {
				employee = employeeDao.readEmployeeId(empId);
			} else {
				System.err.println("Invalid Id");
			}
			
		} else {
			throw new EmployeeException("Employee not found");
		}

		return employee;
	}

	public Employee updateEmployee(Employee employee) throws EmployeeException {
		Employee employee2 = null;
		if (employee != null) {
			employee2 = employeeDao.updateEmployee(employee);
		} else {
			throw new EmployeeException("Employee not updated");
		}
		return employee2;
	}

	public int deleteEmployeeById(int empId) {
		int id = employeeDao.deleteEmployeeById(empId);
		if (id == 1) {
			return 1;
		} else {
			return 0;
		}

	}

}
