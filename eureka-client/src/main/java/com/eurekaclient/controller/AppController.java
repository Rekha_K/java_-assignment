package com.eurekaclient.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "hello")
public class AppController {
	@GetMapping(value = "/world")
	public ResponseEntity<String>  sayHello() {
		String name = "Welcome to Eureka Client!";
		return new ResponseEntity<String>(name, HttpStatus.OK) ;
		
	}

}
