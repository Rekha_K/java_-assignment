package com.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
/**
 * 
 * @author rekha.k
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ CalculatorTest.class, StringNameTest.class })
public class AllTests {

}
