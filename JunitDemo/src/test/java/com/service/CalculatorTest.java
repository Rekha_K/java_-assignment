package com.service;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
/**
 * 
 * @author rekha.k
 *
 */
public class CalculatorTest {
	static Calculator calculator = null;

	/*@Before
	public void startTest() {
		calculator = new Calculator();
		System.out.println("init()");
	}*/
	@BeforeClass
	public static void startTest() {
		calculator = new Calculator();
		System.out.println("@BeforeClass will start only once in a class");
	}

	@Test
	public void testAdd() {

		assertEquals(5, calculator.add(2, 3));
	}

	@Test
	public void testAdd2() {

		assertEquals(8, calculator.add(5, 3));
	}
	
	@Test(expected = ArithmeticException.class)
	public void testDivide() {

		assertEquals(0, calculator.divide(5, 0));
	}
	/*@After
	public void stopTest() {		
		calculator = null;
		System.out.println("stop()");
	}*/
	
	@AfterClass
	public static void stopTest() {
		
		calculator = null;
		System.out.println("@AfterClass will stop only once in a class");
	}

}
