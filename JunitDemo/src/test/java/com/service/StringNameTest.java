package com.service;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;
/**
 * 
 * @author rekha.k
 *
 */
public class StringNameTest {

	@Test
	// @Ignore
	public void test() {
		StringName name = new StringName();
		assertEquals("Welcome", name.stringName());
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)

	public void testGetName() {
		StringName name = new StringName();
		assertEquals("WelcomeRekha", name.getName("Rekha"));
	}

	@Test(expected = NullPointerException.class)
	public void testGetNameNull() {
		StringName name = new StringName();
		assertEquals("WelcomeRekha", name.getName(null));
	}

}
