package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */
public class SetterMain {

	public static void main(String[] args) {

		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/SpringConfig.xml");

		Employee employee = (Employee) applicationContext.getBean("employeeBean");

		System.out.println("Spring  Setter Injection");
		System.out.println("Employee Id :" + employee.getEmpId());
		System.out.println("Employee Name :" + employee.getEmpName());
		System.out.println("Employee Salary :" + employee.getEmpSalary());

		Employee employee1 = new Employee();// control

		System.out.println("Java ");// heap memory address where employee object is created.
		System.out.println("Employee Id :" + employee1.getEmpId());
		System.out.println("Employee Name :" + employee1.getEmpName());
		System.out.println("Employee Salary :" + employee1.getEmpSalary());
		employee1 = null;

	}

}
