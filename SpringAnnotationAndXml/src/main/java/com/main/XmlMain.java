package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Student;

/**
 * 
 * @author rekha.k
 *
 */
public class XmlMain {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/SpringConfig.xml");
		System.out.println("Annotation and XML using Constructor Injection,Setter Injection");
		Student student = (Student) applicationContext.getBean("student");
		System.out.println("Student Id :" + student.getStudNo());
		System.out.println("Student Name :" + student.getStudName());
		System.out.println("Student Mark :" + student.getStudMark());
		System.out.println("Student DepartmentId :" + student.getDepartment().getDepartId());
		System.out.println("Student Departname :" + student.getDepartment().getDepartName());

	}

}
