package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.model.Student;

/**
 * 
 * @author rekha.k
 *
 */
@Configuration
@ComponentScan(basePackages = "com")
public class AnnotationMain {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Student.class);
		Student student = applicationContext.getBean(Student.class);
		System.out.println(student.getStudNo());
		System.out.println(student.getStudName());
		System.out.println(student.getStudMark());
	}

}
