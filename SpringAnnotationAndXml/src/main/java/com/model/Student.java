package com.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * @author rekha.k
 *
 */
@Component
public class Student {
	private int studNo;
	private String studName;
	private int studMark;
	@Autowired
	private Department department;

	public Student() {
		super();
	}

	@Autowired
	public Student(@Value("20") int studNo, @Value("Rechal") String studName, @Value("99") int studMark) {
		super();
		this.studNo = studNo;
		this.studName = studName;
		this.studMark = studMark;
	}

	public int getStudNo() {
		return studNo;
	}

	@Value(value = "18")
	public void setStudNo(int studNo) {
		this.studNo = studNo;
	}

	public String getStudName() {
		return studName;
	}

	@Value(value = "Sarah")
	public void setStudName(String studName) {
		this.studName = studName;
	}

	public int getStudMark() {
		return studMark;
	}

	@Value(value = "90")
	public void setStudMark(int studMark) {
		this.studMark = studMark;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}