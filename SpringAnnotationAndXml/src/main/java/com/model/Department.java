package com.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * @author rekha.k
 *
 */
@Component
public class Department {

	private String departName;

	private int departId;

	public Department() {
		super();
	}

	public Department(String departName, int departId) {
		super();
		this.departName = departName;
		this.departId = departId;
	}

	
	public String getDepartName() {
		return departName;
	}
	@Value("Electrical and Electronics")
	public void setDepartName(String departName) {
		this.departName = departName;
	}

	public int getDepartId() {
		return departId;
	}

	@Value("14")
	public void setDepartId(int departId) {
		this.departId = departId;
	}

}
