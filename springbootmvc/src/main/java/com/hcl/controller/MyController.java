package com.hcl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 
 * @author rekha.k
 *
 */
@Controller // web application not a REST
public class MyController {

	@GetMapping(value = "/hai") // url
	public String sayHello() {

		return "success"; // view name
	}

}
