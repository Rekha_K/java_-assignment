package com.main;

import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Address;
import com.model.Employee;
/**
 * 
 * @author rekha.k
 *
 */
public class OneToManyMain {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/SpringConfig.xml");

		Employee employee = (Employee) applicationContext.getBean("employeeBean");

		System.out.println("Spring One to Many");

		System.out.println("Employee Id :" + employee.getEmpId());
		System.out.println("Employee Name :" + employee.getEmpName());
		System.out.println("Employee Salary :" + employee.getEmpSalary());
		Set<Address> addresses = employee.getAddresses();
		System.out.println("Employee stays in :");
		
		for (Address address : addresses) {
			System.out.println("City :" + address.getCity());
			System.out.println("State :" + address.getState());
			System.out.println("Country :" + address.getCountry());
		}

	}

}
