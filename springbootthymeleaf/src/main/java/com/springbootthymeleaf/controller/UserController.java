package com.springbootthymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.springbootthymeleaf.model.User;

/**
 * 
 * @author rekha.k
 *
 */
@Controller
public class UserController {

	@GetMapping(value = "/")
	public String firstMethod() {

		return "index";// accept user input/Templates/index.html
	}

	@PostMapping(value = "/save")
	public ModelAndView secondMethod(@ModelAttribute User user) { // send the pojo file the view where its displayed

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("success");
		modelAndView.addObject("userData",user);
		return modelAndView;
	}

}
