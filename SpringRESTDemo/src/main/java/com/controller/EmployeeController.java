package com.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.Employee;

@RestController
public class EmployeeController {

	// create Employee
	// @RequestMapping(value = "create", method = RequestMethod.POST)
	@PostMapping(value = "create")
	public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
		System.out.println("Employee Id :" + employee.getEmpNo());
		System.out.println("Employee Name :" + employee.getEmpName());
		System.out.println("Employee created Successfully!");
		ResponseEntity<Employee> responseEntity = new ResponseEntity<Employee>(employee, HttpStatus.CREATED);
		return responseEntity;

	}

	// Read Employee by single Parameter PathVariable
	// @RequestMapping(value = "readbyid/{alias}",method = RequestMethod.GET)
	@GetMapping(value = "readbyid/{alias}")
	public ResponseEntity<Employee> readEmployeeId(@PathVariable("alias") int empId) {// to get only data from user
		Employee employee = null;

		if (empId == 14) { // hard coded instead get value from datasource
			employee = new Employee(14, "fourteen", "Tester");

		}
		if (empId == 15) {	
			employee = new Employee(15, "fifteen", "Tester");

		}
		ResponseEntity<Employee> responseEntity = new ResponseEntity<Employee>(employee, HttpStatus.FOUND);
		return responseEntity;

	}

	// update
	@PutMapping(value = "update/{changeName}") // no need to give alias,if the variable name inside braces same as  parameter.
										
	public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee, @PathVariable String changeName) {

		employee.setEmpName(changeName);

		System.out.println("Employee Name :" + employee.getEmpName());

		ResponseEntity<Employee> responseEntity = new ResponseEntity<Employee>(employee, HttpStatus.OK);
		System.out.println("Employee updated Successfully!");
		return responseEntity;

	}

	// delete Employee by Id
	@DeleteMapping(value = "delete/{empId}")
	public ResponseEntity<Boolean> deleteEmployeeById(@PathVariable int empId) {

		System.out.println("Deleted Employee Id :" + empId);
		ResponseEntity<Boolean> responseEntity = new ResponseEntity<Boolean>(true, HttpStatus.GONE);
		return responseEntity;

	}

}
