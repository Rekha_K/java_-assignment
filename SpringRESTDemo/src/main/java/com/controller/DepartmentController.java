package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.model.Department;
import com.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */
@RestController
public class DepartmentController {
	@RequestMapping(value = "one", method = RequestMethod.GET) // read operation
	public String display() {

		return "Welcome to Spring REST";

	}

	@RequestMapping(value = "two", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }) // read
	// operation
	public ResponseEntity<Department> getDepartment() {

		Department department = new Department(111, "Development", "Chennai");
		Employee employee1 = new Employee(10, "Rekha", "Software Engineer");
		Employee employee2 = new Employee(11, "Jazi", "Software Engineer");
		Employee employee3 = new Employee(12, "Nani", "Software Engineer");
		List<Employee> employees = new ArrayList<Employee>();

		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		department.setEmployee(employees);// will return both department and Employee
		ResponseEntity<Department> responseEntity = new ResponseEntity<Department>(department, HttpStatus.CREATED);
		return responseEntity;
	}

}
