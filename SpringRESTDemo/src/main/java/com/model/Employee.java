package com.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Employee {
	private int empNo;
	private String empName;
	private String designation;

	public Employee() {
		super();
	}

	public Employee(int empNo, String empName, String designation) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.designation = designation;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

}
