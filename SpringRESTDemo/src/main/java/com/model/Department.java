package com.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author rekha.k
 *
 */
@XmlRootElement
public class Department {

	private int deptId;
	private String deptName;
	private String location;
	private List<Employee> employee;// HAS-A (OneToMany)

	public Department() {
		super();
	}

	public Department(int deptId, String deptName, String location) {
		super();
		this.deptId = deptId;
		this.deptName = deptName;
		this.location = location;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public List<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(List<Employee> employee) {
		this.employee = employee;
	}

}
