package com.model;

import java.io.Serializable;
/**
 * 
 * @author rekha.k
 *
 */
public class Employee implements Serializable {
	private int empNo;
	private String empName;
	private float salary;

	public Employee() {
		super();

	}

	public Employee(String empName, float salary) {
		super();
		this.empName = empName;
		this.salary = salary;
	}

	public Employee(int empNo, String empName, float salary) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

}
