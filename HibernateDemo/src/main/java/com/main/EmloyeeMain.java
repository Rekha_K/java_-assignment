package com.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Employee;

/**
 * 
 * @author rekha.k
 *
 */
public class EmloyeeMain {

	public static void main(String[] args) {
		StandardServiceRegistry standardServiceRegistry = new StandardServiceRegistryBuilder().configure().build();
		// StandardServiceRegistryBuilder().configure("com/config/hibernate.cfg.xml").build();
		Metadata meta = new MetadataSources(standardServiceRegistry).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		Transaction transaction = session.beginTransaction();
		Employee employee = new Employee("Jeni", 4444f);
		session.save(employee);
		transaction.commit();
		factory.close();
		session.close();

		// List<Employee> employees = new ArrayList<Employee>();
		/*
		 * List<Employee> list = new ArrayList<Employee>(); try { // Employee employee =
		 * session.get(Employee.class,3);
		 * 
		 * String sqlRead = "select emp.empName from Employee as emp"; List<String>
		 * employees = session.createQuery(sqlRead).list(); for (String name :
		 * employees) { System.out.println("Employee Name :" + name); }
		 * 
		 * Query query = session.createQuery("from Employee");
		 * 
		 * list = query.list(); for (Employee employee2 : list) {
		 * System.out.println("Employee Name :" + employee2.getEmpName());
		 * System.out.println("Employee Id :" + employee2.getEmpNo());
		 * System.out.println("Employee Salary :" + employee2.getSalary());
		 * 
		 * } System.out.println("Hibernate ends");
		 * 
		 * } catch (ObjectNotFoundException e) {
		 * System.out.println("Employee not found"); }
		 */
	}

}
